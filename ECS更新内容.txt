ECS 変更点

・ECSSystemのSystemの処理に必要なコンポーネントの定義方法

	旧:　コンストラクタでAddComponentType<>()を使って定義
	public:
		hogeSystem()
		{
			AddComponentType<hoge1Component>();
			AddComponentType<hoge2Component>();

			~ Systemの初期化処理 ~
		}

	新: マクロを使って自動でenumの定義 & Init関数の定義
	
		DefUseComponentType(hoge1Component,hoge2Component) ※ ; <- セミコロン不要
	public:
		hogeSystem()
		{
			~ Systemの初期化処理 ~
			Init(); <- 必ず呼ぶこと
		}


	DefUseComponentTypeマクロが定義する内容
	例:
		DefUseComponentType(hoge1,hoge2)
			↓
	
		private:
			struct ComponentParamIndex <- SystemのUpdate時に渡されたコンポーネントパラメータから任意のコンポーネントを取得する際に使用
			{
				enum{hoge1,hoge2};
			};
			virtual void Init()override <- 旧VerのAddComponentTypeをマクロで渡された複数タイプ分一括で処理
			{
				AddMultiComponentType<hoge1,hoge2>();
			}

・ECSSystemの各Update関数で渡されたコンポーネントパラメータから任意のパラメータを取得するテンプレート関数をマクロ化

	旧:
	void hogeSystem::UpdateComponents(float delta, UpdateCompParams components)
	{
		auto* hoge= GetCompFromUpdateParam<hogeComponent>(components);
				:	
	}

	新:
	void hogeSystem::UpdateComponents(float delta, UpdateCompParams components)
	{
		auto* hoge= GetCompFromUpdateParam(hogeComponent,components);  <- 引数に取得したいコンポーネントタイプとパラメータを渡す
				:	
	}

	GetCompFromUpdateParamの展開内容はそこまで難しくないのでECSSystem.hの107行目あたりを参照