#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"

// g_Diffuseを単色で出力するピクセルシェーダ
PsOut main(PsIn In)
{
    float4 MateCol = 1; 
    float4 texCol = MeshTex.Sample(WrapSmp, In.UV); 
    MateCol = g_Diffuse * texCol * g_MulColor; 
    clip(MateCol.a - 0.01f);
    PsOut Out = (PsOut)0;
    Out.Color = MateCol;
    return Out;
    
	//PsOut Out;
    //Out.Color = g_Diffuse;
    //return Out;
}
