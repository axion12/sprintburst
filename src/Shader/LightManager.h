#ifndef __LIGHT_MANAGER_H__
#define __LIGHT_MANAGER_H__

#include "Game/Camera/LightCamera.h"

// 各種ライトデータ
//・DirLight		... 平行光源データ 
//・PointLight		... ポイントライトデータ
//・SpotLight		... スポットライトデータ
//・LightManager	... 上記のデータを管理する、ライト管理クラス

// 平行光源
struct DirLight
{
public:
	DirLight()
	{
		Diffuse = ZVec4::one;
		Direction = ZVec3(0, -1, 0);
		Intensity = 1.0f;
	}

	// 平行光源としてデータ設定
	void SetData(const ZVec3& direction, const ZVec4& diffuse)
	{
		Diffuse = diffuse;
		Direction = direction;
		Direction.Normalize();
	}

public:
	ZVec4		Diffuse;	// ライト色
	ZVec3		Direction;	// 方向
	float		Intensity;	// 明るさ Diffuse* Intensityとして使う

};

// ポイントライト
struct PointLight
{
public:
	PointLight()
	{
		Diffuse = ZVec4::one;
		Radius = 1;
		Intensity = 1.0f;
	}

	// 点光源としてデータ設定
	void SetData(const ZVec3& pos, const ZVec4& diffuse, float radius)
	{
		Position = pos;
		Diffuse = diffuse;
		Radius = radius;
	}


public:
	ZVec4			Diffuse;			// 基本色
	ZVec3			Position;			// 座標
	float			Radius;				// 半径
	float			Intensity;			// 明るさ Diffuse* Intensityとして使う

};

// スポットライト
struct SpotLight
{
public:
	SpotLight()
	{
		Diffuse = ZVec4::one;
		Direction = ZVec3(0, 0, 1);
		Range = 10;
		MinAngle = 15;
		MaxAngle = 20;
		Intensity = 1.0f;
	}

	// スポット光源としてデータ設定
	void SetData(const ZVec3& pos, const ZVec3& direction, const ZVec4& diffuse, float range, float minAngle, float maxAngle)
	{
		Position = pos;
		Diffuse = diffuse;
		Direction = direction;
		Direction.Normalize();
		Range = range;
		MinAngle = minAngle;
		MaxAngle = maxAngle;
	}

public:
	ZVec4			Diffuse;	// 基本色
	ZVec3			Position;	// 座標
	ZVec3			Direction;	// 方向
	float			Range;		// 照射距離
	float			MinAngle;	// 角度
	float			MaxAngle;	// 角度
	float			Intensity;	// 明るさ Diffuse* Intensityとして使う

};

// ライト管理クラス
//  シェーダの定数バッファのb5,b6,b7を使用
class LightManager
{
public:
	// 定数
	#include "LightDefines.h"

	#pragma region ModelRenderers用 ライト用定数バッファデータ

	struct cbSampleShaderLight
	{
	public:
		// ライトデータ
		struct DLData {
			ZVec4 Color;	// 色
			ZVec3 Dir;	// 方向
			float _tmp;
			ZVec2 TexelSize;
			ZVec2 gomi;
		};

		// ポイントライトのデータ
		struct PLData {
		public:
			PLData() {
				Radius = 1;
			}

		public:
			ZVec4 Color;		// 色
			ZVec3 Pos;			// 座標
			float Radius;		// 半径
		};

		// スポットライトのデータ
		struct SLData {
			ZVec4 Color;	// 色
			ZVec3 Pos;		// 座標
			float Range;	// 照射範囲

			ZVec3 Dir;		// 方向
			float MinAng;	// min〜maxにかけて、徐々に光の強さが減衰される
			float MaxAng;
			float tmp[3];
		};

	public:
		// 環境光
		ZVec4	AmbientLight;

		// 平行光源
		//// 平行光数
		//int	DL_Cnt = 0;
		//float tmp2[3];
		DLData DL;	// 平行光データ配列(最大3個)

		// ポイントライト
		// ポイントライト数
		int	PL_Cnt = 0;
		float tmp3[3];
		PLData PL[MAX_POINTLIGHT];	// ポイントライトデータ配列(最大100個)

		// スポットライト
		// スポットライト数
		int SL_Cnt = 0;
		float tmp4[3];
		SLData SL[MAX_SPOTLIGHT];	// スポットライトデータ配列(最大100個)


		//	シャドウ変換行列
		ZMatrix	sVP;
	};

	#pragma endregion

public:
	LightManager()
	{
		m_AmbientLight = ZVec3(0.3f, 0.3f, 0.3f);
		m_DirLight = Make_Shared(DirLight,appnew);
	}

	// 初期化
	void Init();

	// 解放
	//  各ライトの管理リストは解放しない
	//  定数バッファなどが解放する
	void Release();

	// 平行光源追加
	//  点光源を追加する場合はこの関数を呼ぶ 誰も保持しなくなった時点で光源は消滅する
	//  消滅時の解除処理はUpdateBeforeDraw()で行う
	//sptr<DirLight> AddDirLight()
	//{
	//	// メモリ確保
	//	sptr<DirLight> add(new DirLight());
	//	// ライトリストへ追加
	//	m_DirLightList.push_back(add);
	//	return add;
	//}

	ZSP<DirLight> GetDirLight()
	{
		return m_DirLight;
	}


	// ポイントライト追加
	//  点光源を追加する場合はこの関数を呼ぶ 誰も保持しなくなった時点で光源は消滅する
	//  消滅時の解除処理はUpdateBeforeDraw()で行う
	ZSP<PointLight> AddPointLight()
	{
		// メモリ確保
		ZSP<PointLight> add(sysnew(PointLight));
		// ポイントライトリストへ追加
		m_PointLightList.push_back(add);
		return add;
	}

	// スポットライト追加
	//  点光源を追加する場合はこの関数を呼ぶ 誰も保持しなくなった時点で光源は消滅する
	//  消滅時の解除処理はUpdateBeforeDraw()で行う
	ZSP<SpotLight> AddSpotLight()
	{
		// メモリ確保
		ZSP<SpotLight> add(sysnew(SpotLight));
		// スポットライトリストへ追加
		m_SpotLightList.push_back(add);
		return add;
	}

	ZSP<LightCamera> GetLightCamera() { return m_Cam; }

	// 更新処理
	//  全てのライト情報を定数バッファに書き込む
	void Update();

	// ライトの定数バッファを直接取得
	auto& GetLightConstantBuffer()
	{
		return m_cb12_Light;
	}


	void ImGui();

public:
	// 環境光
	ZVec3 m_AmbientLight;

	// 平行光源
	//std::list<wptr<DirLight>> m_DirLightList;		// 平行光源管理リスト
	ZSP<DirLight>		m_DirLight;
	
	// 点光源
	ZSList<ZWP<PointLight>>	m_PointLightList;	// ポイントライト管理リスト

	// スポット光源
	ZSList<ZWP<SpotLight>> m_SpotLightList;		// スポットライト管理リスト

	// ModelRenderers用ライト定数バッファ(通常は１フレーム単位での更新)
	ZConstantBuffer<cbSampleShaderLight> m_cb12_Light;

	//	ディレクショナルライト光源のカメラ
	ZSP<LightCamera>		m_Cam;
};


#endif