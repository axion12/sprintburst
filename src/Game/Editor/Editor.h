#pragma once


class Editor
{
public:
	Editor();
	~Editor();

	void Init();
	void Release();


	/*------------------------------------------------*/
	//	シングルトン
	/*------------------------------------------------*/
	inline static Editor& GetInstance() {
		return *m_sInstance;
	}
	static void DeleteInstance() {
		m_sInstance = nullptr;
	}


private:

	static Editor* m_sInstance;

};

#define EditSystem	Editor::GetInstance()