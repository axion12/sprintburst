﻿#ifndef __NETWORK_H__
#define __NETWORK_H__

class Network{
public:
//===============関数===============
	//イベントプロシージャ
	void NetworkEventProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	
	void Init(HWND hWnd);			//初期化
	void MakeSocket(HWND hWnd);		//ソケット生成
	void Release();					//解放


	template<typename T>
	T* GetData();

	void SendData(const char* data,int data_size);			//データ送信

	void ErrorProc(SOCKET sock, const char *func_name);		//エラー処理

//===============構造体=============
	struct Header
	{
		int session_id;
		size_t msg_size;
	};
	struct Header m_Header;

//===============変数================
	


private:
//=======ユーザー定義イベント=======
	static const UINT WM_ASYNC = (WM_USER + 1);
	static const UINT WM_HOST  = (WM_USER + 2);

//===============定数===============
	static const int PORT_NO = 50000;
	static const int BUFSIZE = 1024;

//===============変数===============
	WSADATA m_wsaData;				//WSA

	SOCKET m_Sock;					//通信ソケット
	struct sockaddr_in m_RecvAddr;	//受信アドレス情報
	struct sockaddr_in m_SendAddr;	//送信アドレス情報

	char m_SvName[BUFSIZE];			//IPアドレス・ホスト名
	ULONG m_ServerAddr;				//IPアドレス（バイナリ）
	HANDLE m_hGetHost;				//ハンドル受け取り
	BYTE   m_hostBuf[MAXGETHOSTSTRUCT];
	struct hostent *m_HostEnt;		//ホスト情報構造体

	char m_RecvMsg[BUFSIZE];		//メッセージ受信配列
	char m_SendMsg[BUFSIZE];		//メッセージ送信配列
	int m_SockAddrLen;

	bool m_isSendFlag = false;		//送信可能フラグ

	ZAVector<uint8> m_RecvBuf;


//==========シングルトン==========
private:
	Network() {}

public:
	static Network &GetInstance()
	{
		static Network instance;
		return instance;
	}
//================================

};

#define NETWORK Network::GetInstance()

#endif