﻿#include "MainFrame/ZMainFrame.h"
#include "enet/enet.h"
#include "Network.h"

void Network::NetworkEventProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		//ウィンドウが作成された時
		case WM_CREATE:
			if (enet_initialize() != 0) {
				fprintf(stderr,"ENetの初期化中にエラーが発生しました。\ n");
			}
			atexit(enet_deinitialize);
			break;

		//ホスト名からIPが返ってきた時
		case WM_HOST:
			if (WSAGETASYNCERROR(lParam) != 0) 
			{
				//エラー処理
				MessageBox(hWnd, "サーバのIPアドレスの取得に失敗しました。", "Error", MB_OK | MB_ICONERROR);
				break;
			}
			//ホスト名からIPに変換
			if (m_hGetHost == (HANDLE)wParam) 
			{
				m_HostEnt = (struct hostent *)m_hostBuf;
				m_ServerAddr = *((unsigned long *)((m_HostEnt->h_addr_list)[0]));

				MakeSocket(hWnd);

				//IPアドレス表示
				static char m_msgbuf[BUFSIZE];
				wsprintf(m_msgbuf, "サーバのIPアドレス = %s", inet_ntoa(*(struct in_addr *)m_HostEnt->h_addr_list[0]));
				MessageBox(hWnd, m_msgbuf, "info", MB_OK | MB_ICONINFORMATION);
			}

		//受付ソケットに反応があった時
		case WM_ASYNC:
		{
			switch (WSAGETSELECTEVENT(lParam))
			{
				case FD_READ:	//受信処理
					int messagelen;
					m_SockAddrLen = sizeof(m_RecvAddr);
					messagelen = recv(m_Sock,m_RecvMsg,sizeof(m_RecvMsg),0);
			
					m_RecvMsg[messagelen] = '\0';
					DW_SCROLL(1, "Recv Message %s", m_RecvMsg);

					break;
				case FD_CLOSE:	//終了処理
					Release();
					break;
			}
			break;
		}//end WM_ASYNC
	}//end switch

}

void Network::Init(HWND hWnd)
{

	//WSA初期化
	if (WSAStartup(MAKEWORD(2, 0), &m_wsaData) != 0) 
	{
		//エラー処理
		fprintf(stderr, "Faild in initialization.\n");
		return;
	}
	
	//IPアドレスの取得
	m_isSendFlag = false;
	
	strcpy(m_SvName, "LAPTOP-JL2NVIR3");		//ホスト名
	//strcpy(m_SvName, "DESKTOP-B9VEALL");		
	//strcpy(m_SvName,"192.168.137.10");		//IP直接
	//gethostname(m_SvName,sizeof m_SvName);	//ホスト名自動取得	

	m_ServerAddr = inet_addr((char*)m_SvName);	//文字列からアドレスに変換

	
	if (m_ServerAddr == -1) 
	{	//ホスト名だった場合
		//サーバ名からサーバのホスト情報を取得する
		m_hGetHost = WSAAsyncGetHostByName(hWnd, WM_HOST, m_SvName, (char *)m_hostBuf, sizeof(m_hostBuf));
		if (m_hGetHost == 0)
			MessageBox(hWnd, "サーバのIPアドレスの取得に失敗しました。", "Error", MB_OK | MB_ICONERROR);

		return;
	}
	else 
	{	//IPアドレスだった場合
		MakeSocket(hWnd);
	}


	//IPアドレス表示
	static char m_msgbuf[BUFSIZE];
	wsprintf(m_msgbuf, "サーバのIPアドレス = %s", m_SvName);
	MessageBox(hWnd, m_msgbuf, "info", MB_OK | MB_ICONINFORMATION);


}

void Network::MakeSocket(HWND hWnd)
{
	//ソケットの作成
	m_Sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (m_Sock == INVALID_SOCKET)
	{
		//エラー処理
		ErrorProc(0, "socket()");
	}

	//非同期通信イベント設定
	WSAAsyncSelect(m_Sock, hWnd, WM_ASYNC, FD_READ | FD_CLOSE);

	//送信設定
	memset(&m_SendAddr, 0, sizeof(m_SendAddr));
	m_SendAddr.sin_family = AF_INET;
	m_SendAddr.sin_port = htons(PORT_NO);
	m_SendAddr.sin_addr.S_un.S_addr = m_ServerAddr;

	m_isSendFlag = true;
	
}

void Network::Release() 
{
	closesocket(m_Sock);
	WSACleanup();
}
template<typename T>
T* Network::GetData()
{
	return (T *)m_RecvBuf;
}


void Network::SendData(const char* data,int data_size)
{
	static int HeaderSize;
	HeaderSize = sizeof(struct Header);
	m_Header.msg_size = HeaderSize + data_size;

	memset(m_SendMsg,0,sizeof m_SendMsg);

	memcpy(m_SendMsg,&m_Header,HeaderSize);
	memcpy(m_SendMsg + HeaderSize, data, data_size);
	
	if (m_isSendFlag) 
	{
		sendto(
			m_Sock
			, (const char*)m_SendMsg
			, BUFSIZE
			, 0
			, (struct sockaddr *)&m_SendAddr
			, sizeof(m_SendAddr)
		);
	}
}



void Network::ErrorProc(SOCKET sock, const char *func_name)
{
	LPVOID ErrorMessage;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		0,
		WSAGetLastError(),							//取得エラーメッセージの識別子
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),	//ユーザー定義言語でメッセージを取得する指定
		(LPSTR)&ErrorMessage,						//エラーメッセージバッファへのポインタを取得
		0,
		0);
	fprintf(stderr, "Error : ");
	fprintf(stderr, "%s, ", func_name);
	fprintf(stderr, "%d, ", WSAGetLastError());
	fprintf(stderr, "%s", (char *)ErrorMessage);
	fprintf(stderr, "\n");
	LocalFree(ErrorMessage);

	//ソケットクローズ
	if (m_Sock != 0) closesocket(m_Sock);
	//WSA終了処理
	WSACleanup();
	
	//exit(1);
}