#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../../TestECSComponents/TestComponents.h"
#include "../../TestECSSystems/TestSystems.h"

#include "../../MapObject/MapObjectComponents/MapObjectComponents.h"
#include "../../MapObject/MapObjectSystems/MapObjectSystems.h"

#include "../../Character/CharacterComponents/CharacterComponents.h"
#include "../../Character/CharacterSystems/CharacterSystems.h"

#include "../../CommonECSComponents/CommonListener.h"

#include "TestScene.h"

void TestScene::Release()
{
	// エンティティ削除
	// ※ 削除方法について詳しくはECSEnity.h参照
	ECS.RemoveAllListener();
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();
}

void TestScene::Init()
{
	DW_SCROLL(0, "タイトルシーン初期化");

	// 平行サイト作成
	m_DirLight = ShMgr.m_LightMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(1, -1, -0.5f),		// ライト方向
		ZVec4(0.4f, 0.4f, 0.4f, 1)	// 基本色
	);

	// 環境色
	ShMgr.m_LightMgr.m_AmbientLight.Set(0.1f, 0.1f, 0.1f);

	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);

	
	// MMDモデル描画(PMXテスト PMDは後回し)
	{
		ECS.RegisterClassRefrection<GameModelComponent>("GameModel");
		ECS.RegisterClassRefrection<ModelBoneControllerComponent>("ModelBoneController");
		ECS.RegisterClassRefrection<AnimatorComponent>("Animator");
		ECS.RegisterClassRefrection<TransformComponent>("Transform");
		ECS.RegisterClassRefrection<CameraComponent>("Camera");
		ECS.RegisterClassRefrection<PlayerComponent>("Player");
		ECS.RegisterClassRefrection<PlayerControllerComponent>("PlayerController");
		ECS.RegisterClassRefrection<SurvivorComponent>("Survivor");
		ECS.RegisterClassRefrection<KillerComponent>("Killer");
		ECS.RegisterClassRefrection<SecurityComponent>("Security");
		ECS.RegisterClassRefrection<ServerComponent>("Server");
		ECS.RegisterClassRefrection<ColliderComponent>("Collider");

		ZSP<ModelBoneControllerListener> li = Make_Shared(ModelBoneControllerListener,appnew);
		ECS.AddListener(li.GetPtr());

		// キラー
		auto killerEntity = ECSEntity::CreateEntityFromJsonFile("data/killer.json");
		m_Entities.push_back(killerEntity);
		
		// サバイバー
		auto survivorEntity = ECSEntity::CreateEntityFromJsonFile("data/survivor.json");
		m_Entities.push_back(survivorEntity);

		// セキュリティ
		auto securityEntity = ECSEntity::CreateEntityFromJsonFile("data/security.json");
		m_Entities.push_back(securityEntity);

		// サーバー
		auto serverEntity = ECSEntity::CreateEntityFromJsonFile("data/server.json");
		m_Entities.push_back(serverEntity);

		// マップ
		auto mapEntity = ECSEntity::CreateEntityFromJsonFile("data/testmap.json");
		m_Entities.push_back(mapEntity);

		if (survivorEntity->GetComponent<PlayerControllerComponent>()->Enable)
		{
			m_NowCamera = survivorEntity->GetComponent<CameraComponent>();
		}
		else
		{
			m_NowCamera = killerEntity->GetComponent<CameraComponent>();
		}
	}
	
	//	ポストエフェクトを作成
	m_PostEffect.Init();
	m_PostEffect.LoadState("data/Scene/HkScene/PostState.json");	//	ポストエフェクトステート入力
	
																	//
	m_SkyTex = APP.m_ResStg.LoadTexture("data/Texture/title_back.png");


	ShMgr.m_Blur.CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));


	// システム準備

	m_UpdateSystems.AddSystem(Make_Shared(PlayerUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(KillerUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(SurvivorUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(ColliderSystem,appnew));


	m_UpdateSystems.AddSystem(Make_Shared(SecurityUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(ServerUpdateSystem,appnew));

	//m_UpdateSystems.AddSystem(Make_Shared(MoveUpdateSystem,appnew));
	//m_UpdateSystems.AddSystem(Make_Shared(BoxSpawnerSystem,appnew,m_Entities));
	m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(PhysicsSystem,appnew,APP.m_PhysicsWorld));
	//m_UpdateSystems.AddSystem(Make_Shared(CharaDebugSystem,appnew));
	//m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem,appnew));
	m_DrawSystems.AddSystem(Make_Shared(TestDrawSystem, appnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,appnew));

}

void TestScene::Update()
{
	DW_STATIC(1, "PerformanceTest_Scene");

	// EscでVRSceneへ
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
	//	APP.m_SceneMgr.ChangeScene("VR");
		APP.ExitGameLoop();
		return;
	}
	// 当たり判定の準備
	APP.m_ColEng.ClearList();

	//カメラ操作
	m_Cam.Update();
	ECS.UpdateSystems(m_UpdateSystems,APP.m_DeltaTime);
	APP.m_ColEng.Run();
	
	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());
}

void TestScene::ImGuiUpdate()
{
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("SystemInfo") == false)
		{
			ImGui::End();
			return;
		}

		// 物理エンジン
		{
			bool isEnablePhysicsDebug = APP.m_PhysicsWorld->IsEnableDebugDraw();
			ImGui::Checkbox("Physics Debug Draw", &isEnablePhysicsDebug);
			APP.m_PhysicsWorld->SetDebugDrawMode(isEnablePhysicsDebug);
			ImGui::Separator();
		}

		// 各システム
		auto debugImGui = [](ZSP<ECSSystemBase> system)
		{
			ImGui::Text(system->GetSystemName().c_str());
			system->DebugImGuiRender();
			ImGui::Separator();
		};

		for (auto system : m_UpdateSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		for (auto system : m_DrawSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);
}

void TestScene::Draw()
{
	// 半透明モード
	ShMgr.m_bsAlpha.SetState();



	//	3D描画開始
	RENDERER.Begin3DRendering();
	{


		// 半透明モード
		ShMgr.m_bsAlpha.SetState();

		// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
		ShMgr.m_bsAlpha_AtoC.SetState();

		// カメラやライトのデータをシェーダ側に転送する
		{
			for (auto& entity : m_Entities)
			{
				auto camComp = entity->GetComponent<CameraComponent>();
				auto playerContComp = entity->GetComponent<PlayerControllerComponent>();
				if (camComp)
				{
					if (playerContComp->Enable)
					{
						m_NowCamera = camComp;
					}
				}
			}
			if (m_NowCamera)
			{
				INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), m_NowCamera->Cam.m_IsFPSMode);

				// カメラ行列からビュー行列を作成
				m_NowCamera->Cam.CameraToView();

				// シェーダー側の定数バッファに書き込む
				ShMgr.UpdateCamera(&m_NowCamera->Cam);
			}
			else
			{
				m_Cam.SetCamera();
			}
			// カメラ設定& シェーダに書き込み
			//m_Cam.SetCamera();
			// ライト情報をシェーダ側に書き込む
			ShMgr.m_LightMgr.Update();
			//	モデルシェーダの固定定数を書き込み
			ShMgr.m_Ms.SetConstBuffers();
		}

		// [2D]背景描画
		{
			ShMgr.m_Ss.Begin(false, true);
			ShMgr.m_Ss.Draw2D(m_SkyTex->GetTex(), 0, 0, 1280, 720);
			ShMgr.m_Ss.End();
		}

		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);

		// 物理エンジンのデバッグ描画
		//m_PhysicsWorld->DebugDraw();


		//	シャドウマップ描画	
		ShMgr.SetShadowCamTargetPoint(m_NowCamera->Cam.mCam.GetPos());	//	現在のカメラをセット
		RENDERER.DrawShadow();

		//	3D描画
		ShMgr.m_Ls.Flash();
		ShMgr.m_Ms.Draw();



		//	3Dエフェクト描画

		// ~~~~~~
	}	//	3D描画終了
	RENDERER.End3DRendering();
	//	ポストエフェクト
	m_PostEffect.Execute();


	//// 半透明モード
	//ShMgr.m_bsAlpha.SetState();

	//// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
	//ShMgr.m_bsAlpha_AtoC.SetState();


	//// 描画直前にシェーダのフレーム単位データを更新する
	//// 主にカメラやライトのデータをシェーダ側に転送する
	//{

	//	for (auto& entity : m_Entities)
	//	{
	//		auto camComp = entity->GetComponent<CameraComponent>();
	//		auto playerContComp = entity->GetComponent<PlayerControllerComponent>();
	//		if (camComp)
	//		{
	//			if (playerContComp->Enable)
	//			{
	//				m_NowCamera = camComp;
	//			}
	//		}
	//	}
	//	if (m_NowCamera)
	//	{
	//		INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), m_NowCamera->Cam.m_IsFPSMode);

	//		// カメラ行列からビュー行列を作成
	//		m_NowCamera->Cam.CameraToView();

	//		// シェーダー側の定数バッファに書き込む
	//		ShMgr.UpdateCamera(&m_NowCamera->Cam);
	//	}
	//	else
	//	{
	//		m_Cam.SetCamera();
	//	}

	//	// カメラ設定& シェーダに書き込み
	//	//m_Cam.SetCamera();
	//	// ライト情報をシェーダ側に書き込む
	//	ShMgr.m_LightMgr.Update();
	//}

	//// [2D]背景描画

	//// ~~~~~~

	//// [3D]モデル描画
	//ECS.UpdateSystems(m_DrawSystems,APP.m_DeltaTime,true);

	//// 物理エンジンのデバッグ描画
	////APP.m_PhysicsWorld->DebugDraw();
	//
	//if (APP.m_PhysicsWorld->IsEnableDebugDraw())
	//{
	//	APP.m_ColEng.DebugDraw(1.0f);
	//}
	//
	//ShMgr.m_Ls.Flash();
	//ShMgr.m_Ms.m_InstSMeshRenderer->Flash();
	//ShMgr.m_Ms.m_SkinMeshRenderer->Flash();
	//ShMgr.m_Ms.m_SMeshRenderer->Flash();

}
