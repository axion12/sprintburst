#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../MapObjectComponents/MapObjectComponents.h"
#include "MapObjectSystems.h"

SecurityUpdateSystem::SecurityUpdateSystem()
{
	Init();
	m_DebugSystemName = "SecurityUpdateSystem";
}

void SecurityUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);

	auto securityComp = GetCompFromUpdateParam(SecurityComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController->GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "アクセス")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Box)continue;	// 箱以外は無視

																// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			//auto hitObj = appnew(Collider_Sphere);
			auto hitObj = Make_Shared(Collider_Box, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_3, // 判定する側のフィルタ
				HitShapes::MESH | HitShapes::BOX,
				HitGroups::_3  // 判定される側のフィルタ
			);

			// ボックス情報
			hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize, transComp->Transform);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);
			//hitObj->GetAABB();
			// 登録
			APP.m_ColEng.AddDef(hitObj);	// 判定される側

		}
	}
}

void SecurityUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
}

void SecurityUpdateSystem::DebugImGuiRender()
{
}

ServerUpdateSystem::ServerUpdateSystem()
{
	Init();
	m_DebugSystemName = "ServerUpdateSystem";
}

void ServerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);

	auto serverComp = GetCompFromUpdateParam(ServerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController->GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "アクセス")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Box)continue;	// 箱以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			//auto hitObj = appnew(Collider_Sphere);
			auto hitObj = Make_Shared(Collider_Box, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_3, // 判定する側のフィルタ
				HitShapes::MESH | HitShapes::BOX,
				HitGroups::_3  // 判定される側のフィルタ
			);

			// ボックス情報
			hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize, transComp->Transform);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);
			//hitObj->GetAABB();
			// 登録
			APP.m_ColEng.AddDef(hitObj);	// 判定される側

		}
	}
}

void ServerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
}

void ServerUpdateSystem::DebugImGuiRender()
{
}
