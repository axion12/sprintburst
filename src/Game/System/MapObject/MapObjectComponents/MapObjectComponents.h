#ifndef __MAPOBJECT_COMPONENTS_H__
#define __MAPOBJECT_COMPONENTS_H__

// セキュリティ
DefComponent(SecurityComponent)
{
	bool Enable;
	int Gage;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// サーバー
DefComponent(ServerComponent)
{
	bool Enable;
	int Gage;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};


#endif