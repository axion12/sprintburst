#ifndef __CLOSURETASKMANAGER_H__
#define __CLOSURETASKMANAGER_H__

class ClosureTaskManager
{
public:
	// 全クロージャ実行
	void Update();

	// クロージャ登録
	void AddClosure(const ZString& taskName, std::function<bool()> closure)
	{
		m_List[taskName] = closure;
	}

	// 全クロージャ削除
	void Release()
	{
		m_List.clear();
	}

	// 指定したクロージャ削除
	void DeleteClosure(const ZString& taskName)
	{
		m_List.erase(taskName);
	}

private:
	ZMap<ZString, std::function<bool()> > m_List;

};

#endif // !__CLOSURETASKMANAGER_H__