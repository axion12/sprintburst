#include "MainFrame/ZMainFrame.h"
#include "CharacterComponents.h"

void CameraComponent::InitFromJson(const json11::Json & jsonObj)
{

	Enable = jsonObj["Enable"].bool_value();

	ZVec3 basePos;
	basePos.Set(jsonObj["BasePosition"]);
	
	ZVec3 localPos;
	localPos.Set(jsonObj["LocalPosition"]);
	
	ZVec3 rotate;   
	rotate.Set(jsonObj["Rotation"]);
	Cam.m_RotAngle.x = rotate.x;
	Cam.m_RotAngle.y = rotate.y;

	ZVec2 RotAngleLimit;
	RotAngleLimit.Set(jsonObj["RotAngleLimit"]);
	Cam.m_XRotAngleLimit = RotAngleLimit;

	// ������
	Cam.Init(basePos, localPos);

}

void PlayerComponent::InitFromJson(const json11::Json & jsonObj)
{
	ActionState = jsonObj["ActionState"].int_value();
	MoveSpeed = jsonObj["MoveSpeed"].number_value();
	Gravity = jsonObj["Gravity"].number_value();
}

void PlayerControllerComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = jsonObj["Enable"].bool_value();
}

void KillerComponent::InitFromJson(const json11::Json & jsonObj)
{
}

void SurvivorComponent::InitFromJson(const json11::Json & jsonObj)
{
}


