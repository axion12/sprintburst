#ifndef __CHARACTER_COMPONENTS_H__
#define __CHARACTER_COMPONENTS_H__

#include "Game/Camera/GameCamera.h"


DefComponent(CameraComponent)
{
	GameCamera Cam;

	bool Enable;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// プレイヤー共通
DefComponent(PlayerComponent)
{
	
	int ActionState;	// ステート
	float MoveSpeed;	// 移動速度
	float Gravity;		// 重力
	bool IsSky;			// 空中か
	ZVec3 vMove;		// 移動（保存）


	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

DefComponent(PlayerControllerComponent)
{
	bool Enable;
	ZVec3 Axis;
	int Button;

	enum Buttons
	{
		Action = 0x00000001,
		Run = 0x00000002,
		Squat = 0x00000004,
		Skill = 0x00000008,
		Attack = 0x00000010,
		L2 = 0x00000020,
		R1 = 0x00000040,
		R2 = 0x00000080,
		START = 0x00000100
	};

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// キラー用
DefComponent(KillerComponent)
{

	enum Action
	{
		WAIT,
		WALK,
		ATTACK_SINK,
		ATTACK,
		ATTACK_NOHIT,
		ATTACK_HIT
	};

	ZUnorderedMap<int, int> MultiHitMap;
	bool HitAttack = false;	// 攻撃が当たったか

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// サバイバー用
DefComponent(SurvivorComponent)
{
	enum Action
	{
		WAIT,
		WALK,
		RUN,
		SQUAT_WAIT,
		SQUAT_WALK,
		FALL,
		DOWN_WAIT,
		DOWN_WALK,
		ACCESS,
		CURE
	};

	enum Damage
	{
		NORMAL,
		DAMAGE,
		DOWN
	};

	
	int HP;				// 体力
	int CureGage;		// 負傷・ダウン時の回復ゲージ
	char DamageState;	// ダメージの状態
	bool Squat;			// しゃがんでいるか

	const float WalkSpeed = 0.02f;
	const float RunSpeed = 0.04f;
	const float SquatWalkSpeed = 0.01f;
	const float DownWalkSpeed = 0.01f;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

#endif