#ifndef __CHARACTER_SYSTEMS_H__
#define __CHARACTER_SYSTEMS_H__


// プレイヤーの更新
class PlayerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent, TransformComponent,
		PlayerComponent, CameraComponent,
		PlayerControllerComponent, ColliderComponent)

public:
	PlayerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
	
};

// キラーの更新
class KillerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent, 
		AnimatorComponent,TransformComponent,
		PlayerComponent,CameraComponent,
		PlayerControllerComponent,ColliderComponent,
		KillerComponent)
public:
	KillerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	void Action_Wait(UpdateCompParams components);
	void Action_Walk(UpdateCompParams components);
	void Action_Attack_Sink(UpdateCompParams components);
	void Action_Attack(UpdateCompParams components);
	void Action_Attack_NoHit(UpdateCompParams components);
	void Action_Attack_Hit(UpdateCompParams components);

	void Script_Attack(UpdateCompParams components,json11::Json& scrItem);
private:
	ClosureTaskManager m_CTM;

};

// サバイバーの更新
class SurvivorUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(AnimatorComponent,ModelBoneControllerComponent,
		TransformComponent, PlayerComponent,
		CameraComponent, PlayerControllerComponent,
		ColliderComponent,SurvivorComponent)

public:
	SurvivorUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	void Action_Wait(UpdateCompParams components);
	void Action_Walk(UpdateCompParams components);
	void Action_Run(UpdateCompParams components);
	void Action_SquatWait(UpdateCompParams components);
	void Action_SquatWalk(UpdateCompParams components);
	void Action_Fall(UpdateCompParams components);
	void Action_DownWait(UpdateCompParams components);
	void Action_DownWalk(UpdateCompParams components); 
	void Action_Access(UpdateCompParams components);
	void Action_Cure(UpdateCompParams components);

	
};

#endif