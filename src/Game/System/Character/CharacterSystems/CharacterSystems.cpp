#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../CharacterComponents/CharacterComponents.h"
#include "../../MapObject/MapObjectComponents/MapObjectComponents.h"
#include "CharacterSystems.h"

#include "Game/Camera/GameCamera.h"

KillerUpdateSystem::KillerUpdateSystem()
{
	Init();
	m_DebugSystemName = "KillerUpdateSystem";
}

void KillerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{

	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto killerComp = GetCompFromUpdateParam(KillerComponent,components);

	// 多段ヒットリストの寿命処理
	for (auto it = killerComp->MultiHitMap.begin(); it != killerComp->MultiHitMap.end();)
	{
		(*it).second--;
		if ((*it).second <= 0)
		{
			it = killerComp->MultiHitMap.erase(it);
		}
		else
		{
			++it;
		}
	}

	m_CTM.Update();

	switch (playerComp->ActionState)
	{
	case KillerComponent::WAIT:
		Action_Wait(components);
		break;
	case KillerComponent::WALK:
		Action_Walk(components);
		break;
	case KillerComponent::ATTACK_SINK:
		Action_Attack_Sink(components);
		break;
	case KillerComponent::ATTACK:
		Action_Attack(components);
		break;
	case KillerComponent::ATTACK_NOHIT:
		Action_Attack_NoHit(components);
		break;
	case KillerComponent::ATTACK_HIT:
		Action_Attack_Hit(components);
		break;
	default:
		break;
	}


	// スクリプトキー時に実行される関数
	auto scriptProc = [this,components](ZAnimeKey_Script* scr)
	{
		//Dw_Scroll(2, "スクリプト");

		// 文字列をJsonとして解析
		std::string errorMsg;
		json11::Json jsonObj = json11::Json::parse(scr->Value.c_str(), errorMsg);
		if (errorMsg.size() > 0)
		{
			DW_SCROLL(2, "JsonError（%s）", errorMsg.c_str());
			return;
		}

		auto scriptArray = jsonObj["Scripts"].array_items();
		for (auto& scrItem : scriptArray)
		{
			// スクリプトの種類
			std::string scrType = scrItem["Type"].string_value();
			// 攻撃判定発生
			if (scrType == "Attack")
			{
				Script_Attack(components,scrItem);
			}
		}

	};


	animatorComp->ScriptProc = scriptProc;
}

void KillerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);

	

	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);
		//auto hitObj = appnew(Collider_Ray);
		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::MESH,
			HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 1, 0),	// レイの開始位置
			pos + ZVec3(0, -100, 0));		// レイの終了位置

		APP.m_ColEng.AddAtk(hitObj);
		hitObj->m_OnHitStay = [this, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
		{
			float nearestDist = FLT_MAX;
			//sptr(GameObject> nearestObj;
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					//if (youEntity == nullptr)continue;
					//// 自キャラは無視
					//if (colliderComp->m_Entity == youEntity)continue;
					// 近い？
					if (nearestDist > node.RayDist)
					{
						nearestDist = node.RayDist;
						//nearestObj = youGameObj;
					}
				}
			}

			// 空中時
			if (playerComp->IsSky)	// めりこんでる
			{
				if (nearestDist < 1 && playerComp->vMove.y <= 0)
				{
					transComp->Transform._42 += 1 - nearestDist; // 押し戻す
					playerComp->vMove.y = 0;
					playerComp->IsSky = false;
				}
			}
			else
			{
				if (nearestDist > 1 + 0.3 || playerComp->vMove.y > 0)
				{
					playerComp->IsSky = true;
				}
				else
				{
					transComp->Transform._42 += 1 - nearestDist;
					playerComp->vMove.y = 0;
				}
			}

		};

		// 最後に呼ばれる
		hitObj->m_OnTerminal = [this,bcComp, transComp, cameraComp](const ZSP<ColliderBase>& hitObj)
		{
			// キャラの座標を使用

			auto boneLocalMat = bcComp->BoneController->SearchBone("頭")->LocalMat;
			auto headMat = boneLocalMat * transComp->Transform;

			ZMatrix m;
			m = cameraComp->Cam.m_BaseMat;
			m.Move(headMat.GetPos());

			cameraComp->Cam.mCam = cameraComp->Cam.m_LocalMat * m;

		};
	}


}

void KillerUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

void KillerUpdateSystem::Action_Wait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	
	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		// 移動へ
		playerComp->ActionState = KillerComponent::WALK;
		animatorComp->Animator->ChangeAnimeSmooth("Movement", 0, 10, true);
		return;
	}

	if (playerContComp->Button &
		PlayerControllerComponent::Action)
	{
		// 溜めへ
		playerComp->ActionState = KillerComponent::ATTACK_SINK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	//vTar += cam.m_LocalMat.GetXAxis();
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();


	mat.SetLookTo(vTar, ZVec3::Up);


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Walk(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	
	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		// 待機へ
		playerComp->ActionState = KillerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	if (playerContComp->Button &
		PlayerControllerComponent::Action)
	{
		// 溜めへ
		playerComp->ActionState = KillerComponent::ATTACK_SINK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();

	mat.Move(vTar * playerComp->MoveSpeed);

	ZVec3 vZ = cam.m_LocalMat.GetZAxis();
	vZ.y = 0;
	//vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack_Sink(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components); 
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	
	// アニメーション終了か攻撃ボタンを離したら
	if (animatorComp->Animator->IsAnimationend() ||
		!(playerContComp->Button & PlayerControllerComponent::Action))
	{

		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
		
		// 攻撃へ
		playerComp->ActionState = KillerComponent::ATTACK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack", 0, 3, false);
		cameraComp->Enable = false;

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack(UpdateCompParams components)
{	
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	
	// アニメーション終了（後に攻撃を外したか当てたかに変更）
	if (animatorComp->Animator->IsAnimationend())
	{
		auto killerComp = GetCompFromUpdateParam(KillerComponent, components);

		if (killerComp->HitAttack)
		{
			playerComp->ActionState = KillerComponent::ATTACK_HIT;
			animatorComp->Animator->ChangeAnimeSmooth("Attack(Hit)", 0, 3, false);
			killerComp->HitAttack = false;
		}
		else
		{
			playerComp->ActionState = KillerComponent::ATTACK_NOHIT;
			animatorComp->Animator->ChangeAnimeSmooth("Attack(NoHit)", 0, 3, false);
		}

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack_NoHit(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	
	// アニメーション終了
	if (animatorComp->Animator->IsAnimationend())
	{

		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
		// 待機へ
		playerComp->ActionState = KillerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true); 
		cameraComp->Enable = true;

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack_Hit(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	
	// アニメーション終了
	if (animatorComp->Animator->IsAnimationend())
	{
		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);

		// 待機へ
		playerComp->ActionState = KillerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		cameraComp->Enable = true;

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Script_Attack(UpdateCompParams components,json11::Json& scrItem)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto killerComp = GetCompFromUpdateParam(KillerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);


	struct Param
	{
		// 寿命
		int life;
		// ボーンの名前
		ZString boneName;
		// 当たり判定の半径
		float radius;
		// ヒット間隔
		int hitInterval;

		// ダメージ通知書
		//DamageParam dmg;
	};

	Param param;
	param.life = scrItem["Time"].int_value();
	// ボーンの名前
	param.boneName = scrItem["BoneName"].string_value().c_str();
	// 当たり判定の半径
	param.radius = (float)scrItem["Radius"].number_value();
	// ヒットの間隔
	param.hitInterval = scrItem["HitInterval"].int_value();

	//// ダメージ通知書
	//param.dmg.m_AtkPow = (int)scrItem["AttackRatio"].number_value();
	//param.dmg.m_HitStop = scrItem["HitStop"].int_value();
	//param.dmg.m_HitFlag = (DamageParam::HitFlags)scrItem["Flag"].int_value();
	//auto BlowArray = scrItem["Blow"].array_items();
	//param.dmg.m_vBlow = ZVec3(BlowArray[0].number_value(),
	//	BlowArray[1].number_value(), BlowArray[2].number_value());

	auto task = [this, transComp, bcComp,colliderComp,killerComp, param]()mutable
	{
		param.life--;
		if (param.life <= 0)return false;

		// ボーン
		auto bone = bcComp->BoneController->SearchBone(param.boneName);

		// ボーン行列
		ZMatrix m = bone->LocalMat * transComp->Transform;

		auto hitObj = Make_Shared(Collider_Sphere,appnew);

		// 基本設定
		hitObj->Init(0,
			HitGroups::_5, // 判定する側のフィルタ
			HitShapes::SPHERE | HitShapes::MESH,
			HitGroups::_5  // 判定される側のフィルタ
		);

		// スフィア情報
		hitObj->Set(m.GetPos(), param.radius);

		// 自分のEntityのアドレスを仕込んでおく
		hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

		// デバッグ用の色
		hitObj->m_Debug_Color.Set(1, 0, 0, 1);

		// 登録
		APP.m_ColEng.AddAtk(hitObj);	// 判定される側

		// ヒット時に実行される
		hitObj->m_OnHitStay = [this,colliderComp,killerComp, param](const ZSP<ColliderBase>& hitObj)
		{
			// Hitしたやつら全て
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					killerComp->HitAttack = true;
					// 相手のEntity
					auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (youEntity == nullptr)continue;

					// 自分自身は無視
					//if (colliderComp->m_Entity == youEntity)continue;

					// キャラコンポーネント処理
					//auto youCharaComp = youEntity->GetComponent<Comp_BaseCustom>();
					//if (youCharaComp == nullptr)continue;

					// すでにヒットしているか
					int ID = (int)youEntity;
					if (killerComp->MultiHitMap.count(ID) != 0)continue;

					DW_SCROLL(3, "Hit");
					killerComp->MultiHitMap[ID] = param.hitInterval;



					auto youSurvivorComp = youEntity->GetComponent<SurvivorComponent>();
					auto youPlayerComp = youEntity->GetComponent<PlayerComponent>();
					auto youAnimatorComp = youEntity->GetComponent<AnimatorComponent>();
					youSurvivorComp->DamageState++;

					if (youSurvivorComp->DamageState == SurvivorComponent::DOWN)
					{
						youPlayerComp->ActionState = SurvivorComponent::FALL;

						// しゃがみ
						if (youSurvivorComp->Squat)
							youAnimatorComp->Animator->ChangeAnimeSmooth("SitDown from Down", 0, 10, false);
						else
							youAnimatorComp->Animator->ChangeAnimeSmooth("Stand from Down", 0, 10, false);
					}
					// 結果書
					//DamageReply rep;

					// ダメージ通知を行う
					//if (youCharaComp->OnDamage(YsToSPtr(GetGameObject()), param.dmg, rep))
					//{
					//	if (rep.m_IsGuard)
					//	{
					//		// ガードされた
					//	}
					//	else
					//	{
					//		// ヒットした
					//		m_HitStop = param.dmg.m_HitStop;
					//	}
					//}
				}
			}
		};

		return true;
	};



	m_CTM.AddClosure("Attack", task);
}

SurvivorUpdateSystem::SurvivorUpdateSystem()
{
	Init();
	m_DebugSystemName = "SurvivorUpdateSystem";
}

void SurvivorUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent,components);

	char prevActionState = playerComp->ActionState;

	// しゃがみ
	if (playerContComp->Button & PlayerControllerComponent::Squat)
		survivorComp->Squat = true;
	else
		survivorComp->Squat = false;

	if (INPUT.KeyEnter('I'))
	{
		
		// しゃがみ
		//if (survivorComp->Squat)
		//{
		//	playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;

		//}
		survivorComp->DamageState = SurvivorComponent::NORMAL;
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);


	}
	
	// ダメージ
	switch (survivorComp->DamageState)
	{
	case SurvivorComponent::Damage::DAMAGE:
		break;
	case SurvivorComponent::Damage::DOWN:
		//survivorComp->HP--;
		break;
	default:
		break;
	}


	switch (playerComp->ActionState)
	{
	case SurvivorComponent::WAIT:
		Action_Wait(components);
		break;
	case SurvivorComponent::WALK:
		Action_Walk(components);
		break;
	case SurvivorComponent::RUN:
		Action_Run(components);
		break;
	case SurvivorComponent::SQUAT_WAIT:
		Action_SquatWait(components);
		break;
	case SurvivorComponent::SQUAT_WALK:
		Action_SquatWalk(components);
		break;
	case SurvivorComponent::FALL:
		Action_Fall(components);
		break;
	case SurvivorComponent::DOWN_WAIT:
		Action_DownWait(components);
		break;
	case SurvivorComponent::DOWN_WALK:
		Action_DownWalk(components);
		break;
	default:
		break;
	}

}

void SurvivorUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{

	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);


	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController->GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (survivorComp->DamageState >= SurvivorComponent::DOWN)break;

		if (rb.RigidBodyName == "やられ")
		{

			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

																	// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			//auto hitObj = appnew(Collider_Sphere);
			auto hitObj = Make_Shared(Collider_Sphere, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_5, // 判定する側のフィルタ
				HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
				HitGroups::_5  // 判定される側のフィルタ
			);

			// スフィア情報
			hitObj->Set(m.GetPos(), rb.ShapeSize.x);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);
			//hitObj->GetAABB();
			// 登録
			APP.m_ColEng.AddDef(hitObj);	// 判定される側

		}
		else if (rb.RigidBodyName == "アクセス")
		{
			if (!(playerContComp->Button & PlayerControllerComponent::Action))continue;
			//if (rb.Shape != ZBP_RigidBody::shape::Box)continue;	// 箱以外は無視

																	// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			//auto hitObj = appnew(Collider_Sphere);
			auto hitObj = Make_Shared(Collider_Box, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_3, // 判定する側のフィルタ
				HitShapes::MESH | HitShapes::BOX,
				HitGroups::_3  // 判定される側のフィルタ
			);

			// ボックス情報
			hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize,transComp->Transform);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);
			//hitObj->GetAABB();
			// 登録
			APP.m_ColEng.AddAtk(hitObj);	// 判定される側

			hitObj->m_OnHitStay = [this, playerComp,animatorComp,colliderComp, survivorComp](const ZSP<ColliderBase>& hitObj)
			{
				// Hitしたやつら全て
				for (auto& res : hitObj->m_HitResTbl)
				{
					for (auto& node : res.HitDataList)
					{
						//killerComp->HitAttack = true;
						// 相手のEntity
						auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
						if (youEntity == nullptr)continue;

						// アクセス
						//playerComp->ActionState = SurvivorComponent::ACCESS;
						//animatorComp->Animator->ChangeAnimeSmooth("", 0, 10, true);

						if (youEntity->GetComponent<ServerComponent>())
						{

							DW_SCROLL(3, "Serveraccess");
						}
						else if (youEntity->GetComponent<SecurityComponent>())
						{

							DW_SCROLL(3, "Securityaccess");
						}
					}
				}
			};
		}
	}


	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);
		//auto hitObj = appnew(Collider_Ray);
		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::MESH,
			HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 1, 0),	// レイの開始位置
			pos + ZVec3(0, -100, 0));		// レイの終了位置

		APP.m_ColEng.AddAtk(hitObj);
		hitObj->m_OnHitStay = [this, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
		{
			float nearestDist = FLT_MAX;
			//sptr(GameObject> nearestObj;
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					//if (youEntity == nullptr)continue;
					//// 自キャラは無視
					//if (colliderComp->m_Entity == youEntity)continue;
					// 近い？
					if (nearestDist > node.RayDist)
					{
						nearestDist = node.RayDist;
						//nearestObj = youGameObj;
					}
				}
			}

			// 空中時
			if (playerComp->IsSky)	// めりこんでる
			{
				if (nearestDist < 1 && playerComp->vMove.y <= 0)
				{
					transComp->Transform._42 += 1 - nearestDist; // 押し戻す
					playerComp->vMove.y = 0;
					playerComp->IsSky = false;
				}
			}
			else
			{
				if (nearestDist > 1 + 0.3 || playerComp->vMove.y > 0)
				{
					playerComp->IsSky = true;
				}
				else
				{
					transComp->Transform._42 += 1 - nearestDist;
					playerComp->vMove.y = 0;
				}
			}

		};

		// 最後に呼ばれる
		hitObj->m_OnTerminal = [this, transComp, cameraComp](const ZSP<ColliderBase>& hitObj)
		{
			// キャラの座標を使用
			ZMatrix m;
			m = cameraComp->Cam.m_BaseMat;
			m.Move(transComp->Transform.GetPos());

			cameraComp->Cam.mCam = cameraComp->Cam.m_LocalMat * m;

		};
	}

	
	// カメラのレイ判定
	//{
	//	auto hitObj = Make_Shared(Collider_Ray, appnew);
	//	//auto hitObj = appnew(Collider_Ray);
	//	hitObj->Init(0,
	//		HitGroups::_0,
	//		HitShapes::MESH,
	//		HitGroups::_0);

	//	ZVec3 pos = transComp->Transform.GetPos();
	//	hitObj->Set(pos + ZVec3(0, 1, 0),	// レイの開始位置
	//		cameraComp->Cam.mCam.GetPos());		// レイの終了位置

	//	
	//	APP.m_ColEng.AddAtk(hitObj);
	//	hitObj->m_OnHitStay = [this,cameraComp, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
	//	{

	//		float nearestDist = FLT_MAX;
	//		//sptr(GameObject> nearestObj;
	//		for (auto& res : hitObj->m_HitResTbl)
	//		{
	//			for (auto& node : res.HitDataList)
	//			{
	//				// 近い？
	//				if (nearestDist > node.RayDist)
	//					nearestDist = node.RayDist;
	//			}
	//		}

	//		if (nearestDist < 1)
	//		{
	//			ZVec3 camVec = cameraComp->Cam.mCam.GetPos() - (transComp->Transform.GetPos() + ZVec3(0, 1, 0));

	//			ZVec3 camMove = camVec * (1 - nearestDist);

	//			cameraComp->Cam.m_LocalTransMat.Move(0,0, cameraComp->Cam.m_LocalTransMat.GetPos().z*camMove.z);
	//		}
	//	};

	//}
}

void SurvivorUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

void SurvivorUpdateSystem::Action_Wait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		
		if (survivorComp->Squat)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else if (playerContComp->Button & PlayerControllerComponent::Run)
		{
			// 走り
			playerComp->ActionState = SurvivorComponent::RUN;
			animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->RunSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (survivorComp->Squat)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Walk(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	
	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		if (survivorComp->Squat)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		}
		else
		{
			// 待機
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
			
		}
		return;
	}
	else if (survivorComp->Squat)
	{
		// しゃがみ歩き
		playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
		animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
		playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		return;
	}
	else if (playerContComp->Button & PlayerControllerComponent::Run)
	{
		// 走り
		playerComp->ActionState = SurvivorComponent::RUN;
		animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
		playerComp->MoveSpeed = survivorComp->RunSpeed;
		return;
	}


	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	mat.Move(vTar * playerComp->MoveSpeed);
	
	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);

}

void SurvivorUpdateSystem::Action_Run(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		if (survivorComp->Squat)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		}
		else
		{
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		}
		return;
	}
	else if (!(playerContComp->Button & PlayerControllerComponent::Run))
	{
		if (survivorComp->Squat)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else
		{
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	mat.Move(vTar * playerComp->MoveSpeed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_SquatWait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		if (survivorComp->Squat)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (!survivorComp->Squat)
		{
			// 待機
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
			return;
		}
	}


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_SquatWalk(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);


	if (!survivorComp->Squat)
	{
		if (playerContComp->Axis.x == 0 &&
			playerContComp->Axis.z == 0)
		{
			// 待機
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;

	}
	else
	{
		if (playerContComp->Axis.x == 0 &&
			playerContComp->Axis.z == 0)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	mat.Move(vTar * playerComp->MoveSpeed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 20);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Fall(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	// アニメーション終了（後に攻撃を外したか当てたかに変更）
	if (animatorComp->Animator->IsAnimationend())
	{
		playerComp->ActionState = SurvivorComponent::DOWN_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around Wait", 0, 25, true);
		
		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DownWait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{

		// 歩き
		playerComp->ActionState = SurvivorComponent::DOWN_WALK;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around", 0, 10, true);
		playerComp->MoveSpeed = survivorComp->DownWalkSpeed;
		
		return;
	}


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DownWalk(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);


	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		// 待機
		playerComp->ActionState = SurvivorComponent::DOWN_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around Wait", 0, 10, true);
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	mat.Move(vTar * playerComp->MoveSpeed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Access(UpdateCompParams components)
{

	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{

		if (survivorComp->Squat)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else if (playerContComp->Button & PlayerControllerComponent::Run)
		{
			// 走り
			playerComp->ActionState = SurvivorComponent::RUN;
			animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->RunSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (survivorComp->Squat)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

PlayerUpdateSystem::PlayerUpdateSystem()
{
	Init();
	m_DebugSystemName = "PlayerUpdateSystem";
}

void PlayerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);


	if (INPUT.KeyEnter('P'))
	{
		cameraComp->Cam.m_IsFPSMode = !cameraComp->Cam.m_IsFPSMode;
	}
	if (INPUT.KeyEnter('O'))
	{
		playerContComp->Enable = !playerContComp->Enable;
		cameraComp->Enable = !cameraComp->Enable;
	}

	// カメラ更新
	if (cameraComp->Enable)
		cameraComp->Cam.Update();
	

	// 値をリセット
	playerContComp->Axis.Set(0, 0, 0);
	playerContComp->Button = 0;
	// 無効時
	if (!playerContComp->Enable)return;

	// キーボード入力データ
	if (INPUT.KeyCheck('W', ZInput::Stay))	playerContComp->Axis.z = 1;
	if (INPUT.KeyCheck('S', ZInput::Stay))	playerContComp->Axis.z = -1;
	if (INPUT.KeyCheck('A', ZInput::Stay))	playerContComp->Axis.x = -1;
	if (INPUT.KeyCheck('D', ZInput::Stay))	playerContComp->Axis.x = 1;

	// ボタン
	if (INPUT.KeyCheck(VK_LBUTTON,ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Action;
	if (INPUT.KeyCheck(VK_SHIFT, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Run;
	if (INPUT.KeyCheck(VK_CONTROL, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Squat;
	if (INPUT.KeyCheck('E', ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Skill;
	if (INPUT.KeyCheck(VK_RBUTTON, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Attack;
	//if (INPUT.KeyCheck('S', ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::L2;
	//if (INPUT.KeyCheck('D', ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::R1;
	//if (INPUT.KeyCheck('F', ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::R2;

	if (INPUT.KeyCheck(VK_RETURN, ZInput::Enter)) playerContComp->Button |= PlayerControllerComponent::START;

}

void PlayerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent,components);


	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController->GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "ぶつかり")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

																	// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Sphere,appnew);
			//auto hitObj = appnew(Collider_Sphere);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_0 | HitGroups::_8, // 判定する側のフィルタ
				HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
				HitGroups::_0 | HitGroups::_8 // 判定される側のフィルタ
			);

			// スフィア情報
			hitObj->Set(m.GetPos(), rb.ShapeSize.x);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			//hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 0, 1, 1);

			// 登録
			APP.m_ColEng.AddAtk(hitObj);	// 判定する側
			APP.m_ColEng.AddDef(hitObj);	// 判定される側

			// ヒット時に実行される
			hitObj->m_OnHitStay = [this, transComp,colliderComp](const ZSP<ColliderBase>& hitObj)
			{
				// Hitしたやつら全て
				for (auto& res : hitObj->m_HitResTbl)
				{
					for (auto& node : res.HitDataList)
					{
						//// 相手のEntity
						//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
						//if (youEntity == nullptr)continue;

						//// 自分自身は無視
						//if (colliderComp->m_Entity == youEntity)continue;

						float F = hitObj->CalcMassRatio(node.YouHitObj->m_Mass);
						transComp->Transform.Move(node.vPush*F);
					}
				}
			};
		}
	}

	

}

void PlayerUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}
