#ifndef __COMMON_LISTENER_H__
#define __COMMON_LISTENER_H__

#include "CommonComponents.h"

// ゲーム用モデルのボーンコントローラーリスナー
class ModelBoneControllerListener : public ECSListener
{
public:
	ModelBoneControllerListener()
	{
		AddComponentID(ModelBoneControllerComponent::ID);
		AddComponentID(AnimatorComponent::ID);
		AddComponentID(GameModelComponent::ID);
	}

	virtual ~ModelBoneControllerListener()override {}

	virtual void OnMakeEntity(EntityHandle handle)override{}
	virtual void OnRemoveEntity(EntityHandle hadle)override {}

	virtual void OnAddComponent(EntityHandle handle, uint32 id)override
	{
		auto bcComp = ECS.GetComponent<ModelBoneControllerComponent>(handle);
		auto animComp = ECS.GetComponent<AnimatorComponent>(handle);
		auto modelComp = ECS.GetComponent<GameModelComponent>(handle);

		if (bcComp)
		{
			if (modelComp && animComp)
			{
				bcComp->BoneController->InitAnimator(*animComp->Animator);
				animComp->Animator->ChangeAnime(animComp->InitAnimeName, true);
				animComp->Animator->EnableRootMotion(true);
			}
			else if (modelComp)
			{
				bcComp->BoneController->SetModel(modelComp->Model);
				bcComp->BoneController->AddAllPhysicsObjToPhysicsWorld(*APP.m_PhysicsWorld.GetPtr());
			}
		}

		//if (bcComp && animComp && modelComp)
		//{
		//	bcComp->BoneController->SetModel(modelComp->Model);
		//	bcComp->BoneController->AddAllPhysicsObjToPhysicsWorld(*APP.m_PhysicsWorld.GetPtr());

		//	bcComp->BoneController->InitAnimator(*animComp->Animator);
		//	animComp->Animator->ChangeAnime(animComp->InitAnimeName, true);
		//	animComp->Animator->EnableRootMotion(true);	
		//}
	}
	virtual void OnRemoveComponent(EntityHandle handle, uint32 id)override {}


};

// ColliderComponentのリスナー
class ColliderListener : public ECSListener
{
public:
	ColliderListener()
	{
		AddComponentID(ColliderComponent::ID);
	}

	virtual ~ColliderListener()override {}

	virtual void OnMakeEntity(EntityHandle handle)override {}
	virtual void OnRemoveEntity(EntityHandle hadle)override {}

	virtual void OnAddComponent(EntityHandle handle, uint32 id)override
	{
		auto colliderComp = ECS.GetComponent<ColliderComponent>(handle);

		colliderComp->HitObj->m_UserMap["Entity"] = colliderComp->m_Entity;
	}
	virtual void OnRemoveComponent(EntityHandle handle, uint32 id)override {}


};

#endif