#ifndef __GAME_CAMERA_H__
#define __GAME_CAMERA_H__

// ゲーム用にある程度機能実装したカメラ
class GameCamera : public ZCamera
{
public:
	GameCamera()
	{
		// カメラデータ初期化
		m_IsFPSMode = false;
		m_LocalMat.CreateMove(0, 0, -15);
		SetPerspectiveFovLH(60, (float)ZDx.GetRezoW() / (float)ZDx.GetRezoH(), 0.01f, 1000);

	}

	// 初期設定
	void Init(float rx,float ry,float camZoom,bool isFpsMode = false)
	{
		m_LocalMat.CreateRotateX(rx);
		m_LocalMat.RotateY(ry);
		m_LocalMat.Move_Local(0, 0, camZoom);
		m_BaseMat.CreateMove(0, 0, 0);
		m_IsFPSMode = isFpsMode;
	}

	void Init(const ZVec3& BaseTrans, const ZVec3& LocalTrans,bool isFpsMode = true)
	{
		m_BaseMat.CreateMove(BaseTrans);
		m_LocalTransMat.CreateMove(LocalTrans);
		m_IsFPSMode = isFpsMode;

		INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), m_IsFPSMode);

	}

	// 更新
	void Update()
	{
		// カメラ操作	
		if(APP.m_Window->IsWindowActive() && m_IsFPSMode)
		{
			// 回転
			{
				POINT pt = INPUT.GetMouseMoveValue();

				m_RotAngle.x += pt.y*m_RotRatio;
				m_RotAngle.y += pt.x*m_RotRatio;

				// 角度制限
				if (m_RotAngle.x > m_XRotAngleLimit.x) { m_RotAngle.x = m_XRotAngleLimit.x; }
				if (m_RotAngle.x < m_XRotAngleLimit.y) { m_RotAngle.x = m_XRotAngleLimit.y; }

				// 回転行列
				ZMatrix m_LocalRot;
				ZMatrix mx, my;
				mx.CreateRotateX(m_RotAngle.x);
				my.CreateRotateY(m_RotAngle.y);
					
				m_LocalRot = mx * my;

				m_LocalMat = m_LocalTransMat * m_LocalRot;
			}

		}

	}

	// セット
	void SetCamera()
	{
		// カメラ設定
		// 射影行列設定
		SetProj(mProj);

		// 最終的なカメラ行列を求める
		mCam = m_LocalMat* m_BaseMat;

		// カメラ行列からビュー行列を作成
		CameraToView();

		ZCamera::LastCam = *this;

		// シェーダー側の定数バッファに書き込む
		ShMgr.UpdateCamera(this);
	}



public:
	ZMatrix m_LocalMat;
	ZMatrix m_BaseMat;
	ZMatrix m_LocalTransMat;
	
	ZVec2 m_RotAngle;			// カメラの回転角度
	ZVec2 m_XRotAngleLimit;		// 上下回転の制限（x:下 y:上）
	float m_RotRatio = 0.5;		// カメラの回転量

	bool m_IsFPSMode = true;
};


#endif
