#ifndef __LIGHT_CAMERA_H__
#define __LIGHT_CAMERA_H__

class LightCamera : public ZCamera
{
public:
	/*========================================================================*/
	// Functions
	/*========================================================================*/
	LightCamera();

	void Update();

	void ImGui();

	/*========================================================================*/
	//	accessor
	/*========================================================================*/
	void SetCameraDistance(float dis);
	void SetNearFar(float n, float f);
	void SetCameraDirection(const ZVec3& dir);
	void SetTargetPos(const ZVec3& pos);
	void SetCamPos(const ZVec3& pos);

	/*========================================================================*/
	//	Variable
	/*========================================================================*/
private:
	float Near = 0.1f;
	float Far  = 150.f;
	float ProjW = 100.f;
	float ProjH = 100.f;
//	float Range = 100.f;

	float m_Distance = 50.f;
	ZVec3 m_Direction;
	ZVec3 m_TarPos;
};

#include "LightCamera.inl"

#endif