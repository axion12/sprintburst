#ifndef __ZMMD_FILE_STR_H__
#define __ZMMD_FILE_STR_H__

#include "Utils/ZFile.h"

#include <string>

namespace EzLib
{
	template<size_t size>
	struct ZMMDFileStr
	{
	public:
		ZMMDFileStr()
		{
			Clear();
		}

		void Clear()
		{
			for (auto& c : m_Buffer)
				c = '\0';
		}

		void Set(const char* s)
		{
			size_t i = 0;
			while (i < size && s[i] != '\0')
			{
				m_Buffer[i] = s[i];
				i++;
			}
		
			for (; i < size; i++)
				m_Buffer[i] = '\0';
		}

		const char* c_str()const
		{
			return m_Buffer;
		}

		ZString toStr()const
		{
			return std::move(ZString(m_Buffer));
		}

	
	public:
		char m_Buffer[size + 1];
	};

	template<size_t size>
	static inline bool Read(ZMMDFileStr<size>* str,ZFile& file)
	{
		return file.Read(str->m_Buffer, size);
	}

}


#endif