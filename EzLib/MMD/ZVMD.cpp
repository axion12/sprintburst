#include "ZVMD.h"

#include "Utils/ZFile.h"

namespace EzLib
{
namespace
{
	template<typename T>
	bool Read(T* val, ZFile& file)
	{
		return file.Read(val);
	}

	bool ReadHeader(ZVMD* vmd, ZFile& file)
	{
		Read(&vmd->Header.Header, file);
		Read(&vmd->Header.m_ModelName, file);

		if (vmd->Header.Header.toStr() != "Vocaloid Motion Data 0002" &&
			vmd->Header.Header.toStr() != "Vocaloid Motion Data")
		{
			MessageBox(nullptr, "VMD Header error", "VMD File Load Error",MB_OK);
			return false;
		}

		return !file.IsBad();
	}

	bool ReadMotion(ZVMD* vmd, ZFile& file)
	{
		uint32 motionCnt = 0;
		if (Read(&motionCnt, file) == false)
			return false;

		vmd->Motions.resize(motionCnt);
		for (auto& motion : vmd->Motions)
		{
			Read(&motion.BoneName, file);
			Read(&motion.Frame, file);
			Read(&motion.Translate, file);
			Read(&motion.Quaternion, file);
			Read(&motion.Interpolation, file);
		}

		return !file.IsBad();
	}

	bool ReadBlendShape(ZVMD* vmd, ZFile& file)
	{
		uint32 blendShapeCnt = 0;
		if (Read(&blendShapeCnt, file) == false)
			return false;

		vmd->Morphs.resize(blendShapeCnt);
		for (auto& morph : vmd->Morphs)
		{
			Read(&morph.BlendShapeName, file);
			Read(&morph.Frame, file);
			Read(&morph.Weight, file);
		}

		return !file.IsBad();
	}

	bool ReadCamera(ZVMD* vmd, ZFile& file)
	{
		uint32 cameraCnt = 0;
		if (Read(&cameraCnt, file) == false)
			return false;

		vmd->Cameras.resize(cameraCnt);
		for (auto& camera : vmd->Cameras)
		{
			Read(&camera.Frame, file);
			Read(&camera.Distance, file);
			Read(&camera.Interest, file);
			Read(&camera.Rotate, file);
			Read(&camera.Interpolation, file);
			Read(&camera.ViewAngle, file);
			Read(&camera.IsPerspective, file);
		}

		return !file.IsBad();
	}

	bool ReadLight(ZVMD* vmd, ZFile& file)
	{
		uint32 lightCnt = 0;
		if (Read(&lightCnt, file) == false)
			return false;

		vmd->Lights.resize(lightCnt);
		for (auto& light : vmd->Lights)
		{
			Read(&light.Frame, file);
			Read(&light.Color, file);
			Read(&light.Position, file);
		}

		return !file.IsBad();
	}

	bool ReadShadow(ZVMD* vmd, ZFile& file)
	{
		uint32 shadowCnt = 0;
		if (Read(&shadowCnt, file) == false)
			return false;

		vmd->Shadows.resize(shadowCnt);
		for (auto& shadow : vmd->Shadows)
		{
			Read(&shadow.Frame, file);
			Read(&shadow.ShadowType, file);
			Read(&shadow.Distance, file);
		}

		return !file.IsBad();
	}

	bool ReadIK(ZVMD* vmd, ZFile& file)
	{
		uint32 ikCnt = 0;
		if (Read(&ikCnt, file) == false)
			return false;

		vmd->IKs.resize(ikCnt);
		for (auto& ik : vmd->IKs)
		{
			Read(&ik.Frame, file);
			Read(&ik.Show, file);
			uint32 ikInfoCnt;
			if (Read(&ikInfoCnt, file) == false)
				return false;

			ik.IKInfos.resize(ikInfoCnt);
			for (auto& info : ik.IKInfos)
			{
				Read(&info.Name, file);
				Read(&info.Enable, file);
			}
		}

		return !file.IsBad();
	}

	bool ReadVMDFile(ZVMD* vmd, ZFile& file)
	{
		if (ReadHeader(vmd, file) == false)
		{
			MessageBox(nullptr, "ReadHeader Fail", "VMD File Read Error", MB_OK);
			return false;
		}

		if (ReadMotion(vmd, file) == false)
		{
			MessageBox(nullptr, "ReadMotion Fail", "VMD File Read Error", MB_OK);
			return false;
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadBlendShape(vmd, file) == false)
			{
				MessageBox(nullptr, "ReadBlendShape Fail", "VMD File Read Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadCamera(vmd, file) == false)
			{
				MessageBox(nullptr, "ReadCamera Fail", "VMD File Read Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadLight(vmd, file) == false)
			{
				MessageBox(nullptr, "ReadLight Fail", "VMD File Read Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadShadow(vmd, file) == false)
			{
				MessageBox(nullptr, "ReadShadow Fail", "VMD File Read Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadIK(vmd, file) == false)
			{
				MessageBox(nullptr, "ReadIK Fail", "VMD File Read Error", MB_OK);
				return false;
			}
		}

		return true;
	}

}

	bool ReadVMDFile(ZVMD* vmd,const char* fileName)
	{
		ZFile file;
		if (file.Open(fileName, ZFile::OPEN_MODE::READ) == false)
		{
			MessageBox(nullptr, "VMD File Open Fail", "VMD File Read Error", MB_OK);
			return false;
		}

		return ReadVMDFile(vmd, file);
	}

}