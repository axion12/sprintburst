#include "EzLib.h"

using namespace EzLib;

const ZMatrix ZMatrix::Identity;

// 行列のZ方向をLookDirの方向に向ける。
void ZMatrix::SetLookTo(const ZVec3 &LookDir, const ZVec3 &Up)
{
	using namespace DirectX;

	XMMATRIX mScale = XMMatrixScaling(GetXScale(), GetYScale(), GetZScale());

	ZMatrix R;
	XMVECTOR eyev = XMLoadFloat3(&GetPos());
	XMVECTOR dir = XMLoadFloat3(&LookDir);
	XMVECTOR upv = XMLoadFloat3(&Up);
	auto M = XMMatrixTranspose(XMMatrixLookToLH(eyev, dir, upv));
	M = XMMatrixMultiply(mScale, M);
	XMStoreFloat4x4(&R, M);
	SetRotation(R);

}

// 行列のZ方向をTargetPosの位置を見るように、向ける。
void ZMatrix::SetLookAt(const ZVec3 &TargetPos, const ZVec3 &Up)
{
	using namespace DirectX;

	XMMATRIX mScale = XMMatrixScaling(GetXScale(), GetYScale(), GetZScale());

	ZMatrix R;
	XMVECTOR eyev = XMLoadFloat3(&GetPos());
	XMVECTOR targetv = XMLoadFloat3(&TargetPos);
	XMVECTOR upv = XMLoadFloat3(&Up);
	auto M = XMMatrixTranspose(XMMatrixLookAtLH(eyev, targetv, upv));
	M = XMMatrixMultiply(mScale, M);
	XMStoreFloat4x4(&R, M);
	SetRotation(R);

}

void ZMatrix::RotateLookAtRelative(const ZVec3 &Look)
{
	//===================
	// 追尾のための回転
	//===================
	ZOperationalMatrix mThis(*this);

	// 方向
	ZVec3 vWay;
	vWay.x = _31;
	vWay.y = _32;
	vWay.z = _33;
	vWay.Normalize();
	// 敵への方向
	ZVec3 vTar;
	ZVec3::Normalize(vTar, Look);

	// 内積で角度を求める
	float dot = ZVec3::DotClamp(vWay, vTar);
	if (dot >= -1.0f && dot < 1.0f)
	{
		float rAng = acos(dot);

		// 外積
		ZVec3 crs;
		ZVec3::Cross(crs, vWay, vTar);
		// 正規化
		crs.Normalize();
		if (crs.Length() == 0)
		{
			crs.y = 1;
			rAng = 180;
		}

		ZOperationalMatrix mRota;
		mRota.CreateRotateAxis(crs, rAng);

		// その場回転の為、いったん座標を0にしてから、回転を合成し、その後座標を戻す
		ZVec3 vPos;
		vPos.x = _41;
		vPos.y = _42;
		vPos.z = _43;
		_41 = _42 = _43 = 0;
		mThis *= mRota;
		_41 = vPos.x;
		_42 = vPos.y;
		_43 = vPos.z;

		*this = mThis;
	}
}

void ZMatrix::RotateLookAtRelativeAngle(const ZVec3 &Look, float MaxAng)
{
	//===================
	// 追尾のための回転
	//===================
	ZOperationalMatrix mThis(*this);

	// 方向
	ZVec3 vWay;
	vWay.x = _31;
	vWay.y = _32;
	vWay.z = _33;
	vWay.Normalize();
	// 敵への方向
	ZVec3 vTar;
	ZVec3::Normalize(vTar, Look);


	// 内積で角度を求める
	float dot = ZVec3::DotClamp(vWay, vTar);
	if (dot < 1.0f && dot > -1.0f)
	{
		// 角度制限
		float Deg = DirectX::XMConvertToDegrees(acos(dot));
		if (Deg > MaxAng)
			Deg = MaxAng;

		// 外積
		ZVec3 crs;
		ZVec3::Cross(crs, vWay, vTar);
		if (crs.Length() == 0)return;
		// 正規化
		crs.Normalize();
		if (crs.Length() == 0)
		{
			crs.y = 1;
		}

		ZOperationalMatrix mRota;
		mRota.CreateRotateAxis(crs, Deg);


		// その場回転の為、いったん座標を0にしてから、回転を合成し、その後座標を戻す
		ZVec3 vPos;
		vPos.x = _41;
		vPos.y = _42;
		vPos.z = _43;
		_41 = _42 = _43 = 0;
		mThis *= mRota;
		_41 = vPos.x;
		_42 = vPos.y;
		_43 = vPos.z;

		*this = mThis;
	}
}

void ZMatrix::Lerp(ZMatrix& mOut, const ZMatrix& m1, const ZMatrix& m2, float f)
{
	using namespace DirectX;
	XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m1._11));
	XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m1._21));
	XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m1._31));
	XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m1._41));

	XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m2._11));
	XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m2._21));
	XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m2._31));
	XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&m2._41));

	x1 = XMVectorLerp(x1, y1, f);
	x2 = XMVectorLerp(x2, y2, f);
	x3 = XMVectorLerp(x3, y3, f);
	x4 = XMVectorLerp(x4, y4, f);

	XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&mOut._11), x1);
	XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&mOut._21), x2);
	XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&mOut._31), x3);
	XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&mOut._41), x4);

}

void ZMatrix::Slerp(ZMatrix& mOut, const ZMatrix& m1, const ZMatrix& m2, float f)
{
	// 回転を球面線形補間
	ZQuat q1, q2, q;
	m1.ToQuaternion(q1, true);
	m2.ToQuaternion(q2, true);

	ZQuat::Slerp(q, q1, q2, f);

	// 座標を線形補間
	ZVec3 pos;
	ZVec3::Lerp(pos, m1.GetPos(), m2.GetPos(), f);

	// 拡大を線形補間
	ZVec3 sc;
	ZVec3::Lerp(sc, m1.GetScale(), m2.GetScale(), f);

	// 行列にセット
	q.ToMatrix(mOut);
	mOut.SetPos(pos);
	mOut.Scale_Local(sc);
}

void ZMatrix::ComputeAngle_XYZ(float& angX, float& angY, float& angZ)const
{
	auto _CLAMP = [](float f)
	{
		return max(-1, min(1, f));
	};

	ZMatrix m(*this);
	m.NormalizeScale();

	angY = asin(_CLAMP(-m.m[0][2]));	// -90〜90
	angZ = atan2(m.m[0][1], m.m[0][0]);
	float c = cos(angY);
	if (abs(c) <= 0.001f)
	{
		angZ = atan2(-m.m[1][0], m.m[1][1]);
		angX = 0;
	}
	else
	{
		angX = asin(_CLAMP(m.m[1][2] / c));
		//		if(m.m[2][2] < 0)angX = ToRadian(180) - angX;
		if (m.m[2][2] < 0)
		{
			if (angX > 0)angX = ToRadian(180) - angX;
			else angX = ToRadian(-180) - angX;
		}
	}

	angX = ToDegree(angX);
	angY = ToDegree(angY);
	angZ = ToDegree(angZ);
}

void ZMatrix::ComputePerspectiveInfo(float& outAng, float& outAspect, float& outNear, float& outFar) const
{
	ZVec3 v[3];
	v[0].Set(1, 1, 0);	// Nearの右上
	v[1].Set(1, -1, 0);	// Nearの右下
	v[2].Set(0, 0, 1);	// Far用
	ZMatrix mInvProj = *this;
	mInvProj.Inverse();
	v[0].Transform(mInvProj);
	v[1].Transform(mInvProj);
	v[2].Transform(mInvProj);

	outAspect = v[0].x / v[0].y;

	outNear = v[0].z;
	outFar = v[2].z;

	outAng = DirectX::XMConvertToDegrees(atanf(v[0].y / v[0].z)* 2);

}
