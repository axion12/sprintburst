﻿#include "EzLib.h"

using namespace EzLib;


//====================================================================================================
//
// ZBoneController::ZBCRigidBody
//
//====================================================================================================

ZBoneController::ZBCRigidBody::ZBCRigidBody(ZBoneController& bc) : m_pBC(&bc)
{
}

void ZBoneController::ZBCRigidBody::Release()
{
	ZPhysicsRigidBody::Release();
	m_BoneNode = nullptr;
}

bool ZBoneController::ZBCRigidBody::Create(ZSP<ZPhysicsShape_Base> shape,const ZGM_RigidBodyData& rbData,ZSP<ZGameModel>& model, ZSP<ZBoneController::BoneNode>& node)
{
	Release();

	m_Shape = shape;
	m_CalcType = rbData.PhysicsCalcType;
	m_Group = 1 << rbData.Group;
	m_UnCollisionGroup = rbData.UnCollisionGroup;
	m_BoneNode = node;

	// 剛体の行列は拡大成分を入れてはいけないので、拡大無しの行列と拡大率を分離する
	//  拡大無し行列 → 剛体に使用
	//  拡大率　　　 → 形状に使用
	ZMatrix mat = rbData.GetMatrix();
	ZVec3 scale = mat.GetScale();
	mat.NormalizeScale();

	// 形状に行列の拡大率を適用する
	shape->SetLocalScaling(scale);

	ZSP<ZBoneController::BoneNode> kinematicNode(nullptr);
	bool overrideNode = true;

	// 姿勢算出
	if (m_BoneNode != nullptr)
	{
		m_OffsetMat = mat * m_BoneNode->LocalMat.Inversed();
		kinematicNode = m_BoneNode;
	}
	else
	{
		auto& root = m_pBC->GetBoneTree()[0];
		m_OffsetMat = mat * root->LocalMat.Inversed();
		kinematicNode = root;
		overrideNode = false;
	}

	m_Mass = 0;
	if (m_CalcType != ZGM_RigidBodyData::CalcType::Static)
		m_Mass = rbData.Mass;
	
	// 質量が0以外なら、動的物体なので形状から慣性テンソルを算出。0なら静的物体。
	if (m_Mass != 0.0f)
		m_Shape->GetShape()->calculateLocalInertia(m_Mass, m_Inertia);
	else
		m_Inertia.setValue(0, 0, 0);
	
	btMotionState* motionState = nullptr;

	m_KinematicMotionState = Make_Shared(ZKinematicMotionState,sysnew,kinematicNode, m_OffsetMat);
	if(m_CalcType == ZGM_RigidBodyData::CalcType::Static)
	{
		motionState = m_KinematicMotionState.GetPtr();
	}
	else
	{
		if (m_BoneNode != nullptr)
		{
			if (m_CalcType == ZGM_RigidBodyData::CalcType::Dynamic)
			{
				m_ActiveMotionState = Make_Shared(ZDynamicMotionState,sysnew,kinematicNode, m_OffsetMat);
				motionState = m_ActiveMotionState.GetPtr();
			}
			else if(m_CalcType == ZGM_RigidBodyData::CalcType::DynamicAndBoneMerges)
			{
				m_ActiveMotionState = Make_Shared(ZDynamicAndBoneMerggeMotionState,sysnew,kinematicNode, m_OffsetMat);
				motionState = m_ActiveMotionState.GetPtr();
			}
		}
		else
		{
			m_ActiveMotionState = Make_Shared(ZDefaultMotionState,sysnew,m_OffsetMat);
			motionState = m_ActiveMotionState.GetPtr();
		}
	}

	btRigidBody::btRigidBodyConstructionInfo rbInfo(m_Mass, motionState, m_Shape->GetShape().get(), m_Inertia);
	rbInfo.m_linearDamping = rbData.TranslateDimmer;
	rbInfo.m_angularDamping = rbData.RotateDimmer;
	rbInfo.m_restitution = rbData.Repulsion;
	rbInfo.m_friction = rbData.Friction;
	rbInfo.m_additionalDamping = true;

	m_Body = sptr<btRigidBody>(new btRigidBody(rbInfo));
	m_Body->setUserPointer(this);
	m_Body->setSleepingThresholds(0.01f, ToRadian(0.1f));
	m_Body->setActivationState(DISABLE_DEACTIVATION);

	if (m_Mass == 0 || m_CalcType == ZGM_RigidBodyData::CalcType::Static)
		SetKinematic(true);
	
	return true;
}

void ZBoneController::ZBCRigidBody::RefrectGlobalTransform()
{
	if (m_ActiveMotionState != nullptr)
		m_ActiveMotionState->ReflectGlobalTransform();

	if (m_KinematicMotionState != nullptr)
		m_KinematicMotionState->ReflectGlobalTransform();
}

void ZBoneController::ZBCRigidBody::CalcLocalTransfrom()
{
	if (m_BoneNode == nullptr)
		return;

	// 親がいないなら
	if (m_BoneNode->Mother.IsActive() == false)
	{
		m_BoneNode->TransMat = m_BoneNode->LocalMat;
		return;
	}
	
	auto parent = m_BoneNode->Mother.Lock();
	ZMatrix localMat = m_BoneNode->LocalMat * parent->LocalMat.Inversed();
	m_BoneNode->TransMat = localMat;
}

//====================================================================================================
//
// ZBoneController
//
//====================================================================================================
bool ZBoneController::LoadMesh(const ZString& filename)
{
	// リソースストレージを使用して、モデルデータ読み込み
	ZSP<ZGameModel> pGm = ZDx.GetResStg()->LoadMesh(filename);

	return SetModel(pGm);
}

void ZBoneController::SetRefMatrix(ZSP<ZMatrix> mat)
{
	if (mat == nullptr)
		return;
	
	m_RefMat = mat;
	//for (auto objectSet : m_PhysicsObjectSetList)
	//{
	//	for (auto rb : objectSet.RigidBodys)
	//	{
	//		ZMatrix tmp;
	//		auto btTrans = rb->GetActiveMotionState()->GetInitTrans();
	//		btTrans.getOpenGLMatrix(&tmp._11);
	//		tmp = *mat * (mat->Inversed() * tmp);
	//		rb->SetMatrix(tmp);
	//	}
	//}

}

void ZBoneController::ResetRefMatrix()
{
	m_RefMat.Reset();

	//for (auto objectSet : m_PhysicsObjectSetList)
	//{
	//	for (auto rb : objectSet.RigidBodys)
	//	{
	//			
	//	}
	//}


}

bool ZBoneController::SetModel(ZSP<ZGameModel> pGameMesh, bool enableBones)
{
	// 解放
	Release();

	// メッシュがnullptrなら何もしない
	if (pGameMesh == nullptr)
		return false;

	// ZGameModelのアドレス登録
	m_pGameModel = pGameMesh;
	if (m_pGameModel->GetRoot() == nullptr)
		return false;

	// ボーンを使わないなら
	if (enableBones == false)
		return true;

	// ボーンツリー作成
	CreateBoneTreeFromModel();
	recCalcBoneMatrix(*m_BoneTree[0], ZMatrix::Identity);

	// スキンメッシュの場合のみ、ボーン用定数バッファ作成
	if (m_pGameModel->GetModelTbl_Skin().size() >= 1)
		m_cb_Bones.CreateBySize(3, sizeof(ZMatrix)* m_BoneTree.size());

	// 物理オブジェクト作成(剛体,ジョイントの作成のみ)
	if (CreatePhysicsObjectsFromModel() == false)
		return false;

	return true;
}

void ZBoneController::ResetDefaultTransMat()
{
	for (UINT i = 0; i < m_BoneTree.size(); i++)
		m_BoneTree[i]->TransMat = m_BoneTree[i]->pSrcBoneNode->DefTransMat;
}

void ZBoneController::EnableIKBone(const ZString& IKBoneName, bool flg)
{
	if (m_IKs.empty())
		return;

	auto& it = std::find_if(m_IKs.begin(), m_IKs.end(),
		[IKBoneName](auto& ik)
	{
		return IKBoneName == ik->GetName();
	});

	if (it != m_IKs.end())
		(*it)->SetEnable(flg);
}

bool ZBoneController::IsEnableIK(const ZString & IKBoneName)
{
	if (m_IKs.empty())
		return false;

	auto& it = std::find_if(m_IKs.begin(), m_IKs.end(),
							[IKBoneName](auto& ik)
	{
		return IKBoneName == ik->GetName();
	});

	if (it != m_IKs.end())
		return (*it)->GetEnabled();

	return false;
}

void ZBoneController::CreateBoneTreeFromModel()
{
	// 再帰関数
	std::function<void(ZSP<ZGameModel::BoneNode>, ZSP<BoneNode>, int)> recCreateBoneTree = [this, &recCreateBoneTree](ZSP<ZGameModel::BoneNode> pBoneNode, ZSP<BoneNode> pBoneTreeNode, int Level)
	{
		// ツリーに登録
		m_BoneTree.push_back(pBoneTreeNode);

		// 情報セット
		pBoneTreeNode->pSrcBoneNode = pBoneNode;
		pBoneTreeNode->Level = Level;
		// 子を再帰
		for (UINT i = 0; i < pBoneNode->Child.size(); i++)
		{
			// 子登録
			ZSP<BoneNode> ftn = Make_Shared(BoneNode,appnew);
			ftn->Mother = pBoneTreeNode;
			pBoneTreeNode->Child.push_back(ftn);

			// 再帰
			recCreateBoneTree(pBoneNode->Child[i], ftn, Level + 1);
		}
	};
	// 実行
	recCreateBoneTree(m_pGameModel->GetBoneTree()[0], Make_Shared(BoneNode,appnew), 0);

	// とりあえず、全ボーンのデフォルトの変換行列もコピーしとく
	for (UINT i = 0; i < m_BoneTree.size(); i++)
		m_BoneTree[i]->TransMat = m_BoneTree[i]->pSrcBoneNode->DefTransMat;

	// PMDまたはPMX形式ファイルはツリー構造に変換してファイルに書き出されてない(?)
	// そのため、読み込んだボーンデータをツリー構造に格納した際に配列に格納される順番がバラける。
	// そのばらけた並びのままシェーダーの定数バッファに渡すと、意図しないボーン行列を参照してしまい、
	// アニメーションさせるとメッシュがカオスな形状で描画されてしまうので、
	// ↑で情報をセットしたボーンツリー配列を参照先ボーンIndex順にソートした配列を用意する
	m_SortedBones.resize(m_BoneTree.size());
	for (auto& bone : m_BoneTree)
		m_SortedBones[bone->pSrcBoneNode->OffsetID] = bone;

	// 物理ワールド更新前に計算するノードと更新後に計算するノードで仕分け
	for (auto& bone : m_BoneTree)
	{
		if (bone->pSrcBoneNode->DeformAfterPhysics)
			m_DeformAfterPhysicsBones.push_back(bone);
		else
			m_DeformBeforePhysicsBones.push_back(bone);
	}

	// IK登録
	for (auto& bone : m_BoneTree)
	{
		if (bone->pSrcBoneNode->BF_IsIK() == false)
			continue;

		auto* solver = sysnew(ZIKSolver,*this);
		solver->SetIKNode(bone);
		{
			ZString BoneName = m_pGameModel->GetBoneTree()[bone->pSrcBoneNode->IK.boneIdx]->BoneName;
			auto it = std::find_if(m_SortedBones.begin(), m_SortedBones.end(),
								   [&BoneName](auto& bone)
			{
				return BoneName == bone->pSrcBoneNode->BoneName;
			});
			solver->SetTargetNode((*it));
		}

		for (const auto& ikLink : bone->pSrcBoneNode->IK.LinkList)
		{
			ZString BoneName = m_pGameModel->GetBoneTree()[ikLink.boneIdx]->BoneName;
			auto it = std::find_if(m_SortedBones.begin(), m_SortedBones.end(),
								   [&BoneName](auto& bone)
			{
				return BoneName == bone->pSrcBoneNode->BoneName;
			});

			if (ikLink.bLimitAng)
				solver->AddIKChain((*it), true, ikLink.minLimitAng, ikLink.maxLimitAng);
			else
				solver->AddIKChain((*it));

			solver->SetEnable(true);
		}

		solver->SetIterateCount(bone->pSrcBoneNode->IK.LoopCnt);
		solver->SetLimitAngle(bone->pSrcBoneNode->IK.ikLimitedAng);
		m_IKs.emplace_back(solver);
	}

}

bool ZBoneController::CreatePhysicsObjectsFromModel()
{
	if (m_pGameModel == nullptr)
		return false;

	auto& physicsDataSetList = m_pGameModel->GetPhysicsDataSetList();

	m_PhysicsObjectSetList.resize(physicsDataSetList.size());

	for (size_t i = 0;i < physicsDataSetList.size();i++)
	{
		auto& physicsDataSet = physicsDataSetList[i];

		// 剛体
		for (auto& rbData : physicsDataSet.RigidBodyDataTbl)
		{
			ZBCRigidBody* rb = nullptr;
			if (CreateRigidBody(&rb, rbData,m_pGameModel,*this) == false)
				return false;
			
			ZMatrix mat = rbData.GetMatrix();
			
			m_PhysicsObjectSetList[i].RigidBodys.emplace_back(rb);
		}
		
		auto& rbs = m_PhysicsObjectSetList[i].RigidBodys;

		// ジョイント
		for (auto& jointData : physicsDataSet.JointDataTbl)
		{
			auto& indexA = jointData.RigidBodyAIndex;
			auto& indexB = jointData.RigidBodyBIndex;

			// indexが正常値でなければ無視
			if ((indexA == indexB) ||
				(indexA < 0 || indexA >= (int)rbs.size()) ||
				(indexB < 0 || indexB >= (int)rbs.size()))
				continue;

			auto& rbA = rbs[indexA];
			auto& rbB = rbs[indexB];

			ZPhysicsJoint_Base* joint = nullptr;
			// ジョイント生成に知っばいしたら
			if (CreateJoint(&joint, jointData, rbA->GetBody().get(), rbB->GetBody().get()) == false)
				return false;

			m_PhysicsObjectSetList[i].Joints.emplace_back(joint);
		}
	}

	return true;
}

void ZBoneController::CalcBoneMatrix(bool afterPhysics)
{
	if (m_BoneTree.empty())return;

	if (afterPhysics)
		UpdatePhysics();

	// ボーンを計算していく
	if (afterPhysics)
	{
		if (m_DeformAfterPhysicsBones.size() <= 0)
			return;

		for(size_t i = 0;i < m_DeformAfterPhysicsBones.size();i++)
		{
			ZMatrix parent = ZMatrix::Identity;
			if (m_DeformAfterPhysicsBones[i]->Mother.IsActive())
				parent = m_DeformAfterPhysicsBones[i]->Mother.Lock()->LocalMat;
			else
			{
				if (m_RefMat.IsActive())
					parent = *m_RefMat.Lock();
			}
			parent.NormalizeScale();
			recCalcBoneMatrix(*m_DeformAfterPhysicsBones[i], parent);
		}
	}
	else
	{
		ZMatrix parent = ZMatrix::Identity;
		if (m_RefMat.IsActive())
			parent = *m_RefMat.Lock();
		parent.NormalizeScale();
		recCalcBoneMatrix(*m_DeformBeforePhysicsBones[0], parent);

		// IK計算
		//for (auto& ik : m_IKs)
			//ik->Solve();
	}

}

void ZBoneController::UpdatePhysics()
{
	for (auto& physicsObjSet : m_PhysicsObjectSetList)
	{
		auto& rbs = physicsObjSet.RigidBodys;
		for (auto& rb : rbs)
		{
			rb->RefrectGlobalTransform();
			rb->CalcLocalTransfrom();
		}
	}

	recCalcBoneMatrix(*m_BoneTree[0], ZMatrix::Identity);
}

void ZBoneController::UpdateBoneConstantBuffer()
{
	// チェック
	if (m_cb_Bones.IsInit() == false)return;

	// ボーンのワールド行列から、オフセットワールド行列に変換しセットする
	for (UINT bn = 0; bn < m_SortedBones.size(); bn++)
		ZMatrix::Multiply(m_cb_Bones.m_Data.mWArray[bn], m_SortedBones[bn]->pSrcBoneNode->OffsetMat, m_SortedBones[bn]->LocalMat);

	// 実際に書き込む
	m_cb_Bones.WriteData();

}

void ZBoneController::Release()
{
	m_BoneTree.clear();
	m_cb_Bones.Release();
	m_SortedBones.clear();
	
	for (auto& physicsObjSetList : m_PhysicsObjectSetList)
	{
		for (auto& joint : physicsObjSetList.Joints)
		{
			joint->Release();
		}
		physicsObjSetList.Joints.clear();

		for (auto& rb : physicsObjSetList.RigidBodys)
			rb->Release();
		physicsObjSetList.RigidBodys.clear();
	}

	m_PhysicsObjectSetList.clear();
	m_pGameModel = nullptr;
}

void ZBoneController::InitAnimator(ZAnimator& animator)
{
	// アニメータ初期化
	animator.Init();

	// アニメリストコピー
	if (m_pGameModel)
		animator.CopyAnimationList(m_pGameModel->GetAnimeList());

	// フレームアニメデータ構築
	animator.ClearRefMatrix();
	for (UINT i = 0; i < m_BoneTree.size(); i++)
		animator.AddRefMatrix(&m_BoneTree[i]->TransMat);
}

void ZBoneController::AddAllPhysicsObjToPhysicsWorld(ZPhysicsWorld & world)
{
	if (m_PhysicsObjectSetList.size() <= 0)
		return;

	int numSets = m_PhysicsObjectSetList.size();
	auto& physicsDataSetList = m_pGameModel->GetPhysicsDataSetList();
	for (int si = 0; si < numSets; si++)
	{
		auto& physicsObjSet = m_PhysicsObjectSetList[si];
		auto& physicsDataSet =  physicsDataSetList[si];
		if (physicsObjSet.RigidBodys.size() <= 0 ||
			physicsDataSet.RigidBodyDataTbl.size() <= 0)
			continue;

		// 剛体を物理ワールドに追加
		int numRbs = physicsObjSet.RigidBodys.size();
		for (int ri = 0; ri < numRbs; ri++)
		{
			auto& rb = physicsObjSet.RigidBodys[ri];
			if (rb->AddToWorld(world, rb->GetGroup(),rb->GetUnCollisionGroup()) == false)
			{
				// ~~エラー処理~~
				// (未実装)
				return;
			}
		}

		// ジョイントを物理ワールドに追加
		int numJoints = physicsObjSet.Joints.size();
		for (int ji = 0; ji < numJoints; ji++)
		{
			auto& joint = physicsObjSet.Joints[ji];
			
			joint->AddToWorld(world, false);
		}

	}

}


bool EzLib::CreateRigidBody(ZBoneController::ZBCRigidBody** out, ZGM_RigidBodyData& rbData, ZSP<ZGameModel>& model, ZBoneController& bc)
{
	Safe_Delete(*out);

	ZSP<ZPhysicsShape_Base> shape;

	CreateShape(shape, rbData);

	*out = sysnew(ZBoneController::ZBCRigidBody,bc);

	ZSP<ZBoneController::BoneNode> node(nullptr);

	if (rbData.BoneIndex > 0 || rbData.BoneIndex < (int)bc.GetSortedBones().size())
		node = bc.GetSortedBones()[rbData.BoneIndex];

	if ((*out)->Create(shape, rbData, model, node) == false)
	{
		shape = nullptr;
		Safe_Delete((*out));
		return false;
	}

	return true;
}


//====================================================================================================
//
// ZIKSolver
//
//====================================================================================================
// IK計算用
namespace
{
	float Clamp(const float f, const float minf, const float maxf)
	{
		return min(max(minf, f), maxf);
	}
}

ZIKSolver::ZIKSolver(ZBoneController& bc)
		: m_BC(bc),
		m_IKNode(nullptr),
		m_IKTarget(nullptr),
		m_IterateCnt(1),
		m_LimitAng((float)M_PI * 2.0f),
		m_IsEnable(true)
{
}

void ZIKSolver::Release()
{
	m_Chains.clear();
}

void ZIKSolver::AddIKChain(ZSP<BoneNode> node, bool isKnee)
{
	IKChain chain;
	chain.Node = node;
	chain.EnableAxisLimit = isKnee;
	// 膝なら
	if (isKnee)
	{
		chain.LimitMin = ZVec3(ToRadian(0.5f), 0, 0);
		chain.LimitMax = ZVec3(ToRadian(180.0f), 0, 0);
	}

	AddIKChain(std::move(chain));
}

void ZIKSolver::AddIKChain(ZSP<BoneNode> node, bool axisLimit, const ZVec3& limitMin, const ZVec3& limitMax)
{
	IKChain chain;
	chain.Node = node;
	chain.EnableAxisLimit = axisLimit;
	chain.LimitMin = limitMin;
	chain.LimitMax = limitMax;
	AddIKChain(std::move(chain));
}

void ZIKSolver::Solve()
{
	if (m_IsEnable == false)
		return;
	const float errToleranceSq = 0.000001f;
	for (uint32 i = 0; i < m_IterateCnt; i++)
	{
		_Solve(i);
		// IKノードとIKターゲットノードとの距離の2乗が 0.000001fより小さければIK終了
		{
			const auto targetPos = m_IKTarget->LocalMat.GetPos();
			const auto ikPos = m_IKNode->LocalMat.GetPos();
			float distSq = (targetPos - ikPos).LengthSq();
			if (distSq < errToleranceSq)
				break;
		}
	}
	
}

void ZIKSolver::UpdateBone(BoneNode& bone)
{
	if (bone.Mother.IsActive())
	{
		auto mother = bone.Mother.Lock()->LocalMat;
		m_BC.recCalcBoneMatrix(bone, mother);
	}
	else
		m_BC.recCalcBoneMatrix(bone, ZMatrix::Identity);
}

void ZIKSolver::AddIKChain(IKChain&& chain)
{
	m_Chains.emplace_back(chain);
}

void ZIKSolver::_Solve(uint32 iteration)
{
	const ZVec3 eps0(0.000001f);
	
	for(size_t i = 0;i<m_Chains.size();i++)
	{
		auto chain = m_Chains[i];

		if (chain.Node == m_IKTarget)
			continue;

		ZVec3 ikNodePos = m_IKNode->LocalMat.GetPos();
		ZVec3 targetPos = m_IKTarget->LocalMat.GetPos();
	
		// ボーンのローカル座標に変換
		ZMatrix invMat = chain.Node->LocalMat.Inversed();
		ZVec3 target = targetPos;
		ZVec3 ikPos = ikNodePos;
		target.Transform(invMat);
		ikPos.Transform(invMat);
	
		// 目的方向算出
		ZVec3 ikDir = ikPos.Normalized();
		ZVec3 tgDir = target.Normalized();
		if (ZVec3::NearEqual(tgDir, ikDir, eps0))
			continue;
	
		// 回転角度 & 角度
		float dot = ZVec3::DotClamp(tgDir, ikDir);
		float ang = acos(dot);
		
		// 1度に回転させる角度を単位各で制限
		Clamp(ang, -m_LimitAng, m_LimitAng);

		ZVec3 rotAxis = ZVec3::Cross(tgDir, ikDir);
		if (ZVec3::NearEqual(rotAxis, ZVec3::Zero, eps0))
			continue;
		rotAxis.Normalize();
	
		// tgDirをikDirに一致させるための回転
		ZQuat rot;
		rot.RotateAxis(rotAxis, ToDegree(ang));
	
		ZQuat ikRot = chain.Node->TransMat.ToQuaternion();
		ZQuat::Multiply(ikRot,rot,ikRot);
		
		// 回転制限
		ZMatrix mat = ikRot.ToMatrix();
	
		if(chain.EnableAxisLimit)
		{
			float rx, ry, rz;
			mat.ComputeAngle_XYZ(rx, ry, rz);
			ZVec3 rot_xyz(ToRadian(rx), ToRadian(ry), ToRadian(rz));
			rot_xyz.Clamp(chain.LimitMin, chain.LimitMax);
			mat.CreateRotateX(ToDegree(rot_xyz.x));
			mat.RotateY(ToDegree(rot_xyz.y));
			mat.RotateZ(ToDegree(rot_xyz.z));
		}
	
		chain.Node->TransMat.SetRotation(mat);

		UpdateBone(*chain.Node);
	
	}
	
}
