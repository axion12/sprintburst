//===============================================================
//  @file ZConstantBuffer.h
//   定数バッファクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZConstantBuffer_h
#define ZConstantBuffer_h

namespace EzLib
{

	//==============================================================
	// 
	//   コンスタント(定数)バッファ テンプレートクラス
	// 
	//  [主な機能]														\n
	// 	・Create()で指定クラス・構造体をコンスタントバッファとして作成	\n
	// 	・WriteData()でバッファに書き込み								\n
	// 	・SetVS()やSetPS()などでデバイスへセット						\n
	// 
	//  @ingroup Graphics_Shader
	//==============================================================
	template<typename T>
	class ZConstantBuffer
	{
	public:

		//   定数バッファデータ　WriteData関数でこいつの内容が書き込まれる
		T			m_Data;

		//   バッファの種類
		enum class BufType
		{
			None,			// なし
			Default,		// GPUからWrite○ Read○　CPUからWrite×Read×　(ただしUpdateSubresource()で更新は可能)　高速
			Dynamic,		// GPUからWrite× Read○　CPUからWrite○Read×　頻繁に更新されるようなバッファはこっちの方が効率いいが、Defaultに比べたら少し速度は落ちる
		};

		//   初期化されているか
		bool IsInit() { return m_pBuffer == nullptr ? false : true; }

		//--------------------------------------------------------
		//   テンプレートで指定された型で定数バッファを作成
		//   StartSlot : 定数バッファをセットする場所(HLSLのregister(b0)とかの、b0のところの番号　SetVS(no)のところで指定できるので適当でも良い)
		//   isDynamic : 動的バッファとして作成する(頻繁に更新する場合はこれがいい)
		//  @return 作成成功:true
		//--------------------------------------------------------
		bool Create(UINT StartSlot, bool isDynamic = false);

		//--------------------------------------------------------
		//   指定サイズで定数バッファを作成
		//   StartSlot : 定数バッファをセットする場所(HLSLのregister(b0)とかの、b0のところの番号　SetVS(no)のところで指定できるので適当でも良い)
		//   byteSize : バイトサイズ(16の倍数バイトでないと失敗します)
		//   isDynamic : 動的バッファとして作成する(頻繁に更新する場合はこれがいい)
		//  @return 作成成功:true
		//--------------------------------------------------------
		bool CreateBySize(UINT StartSlot, int byteSize, bool isDynamic = true);

		//--------------------------------------------------------
		//   解放
		//--------------------------------------------------------
		void Release();

		//--------------------------------------------------------
		//   m_Dataを定数バッファに書き込む
		//  	writeBytes … 書き込むバイト数 -1で全部　※だたしdynamic = trueで作成された時のみ有効。定数バッファの書き換え自体は全領域になります。
		//--------------------------------------------------------
		void WriteData(int writeBytes = -1);

		//--------------------------------------------------------
		//   頂点シェーダに設定
		//   slotNo … 定数バッファをセットする場所(HLSLのregister(b0)とかの、b0のところの番号) -1でCreate()したときに指定した番号
		//--------------------------------------------------------
		void SetVS(int slotNo = -1)
		{
			if (m_pBuffer == nullptr)return;

			if (slotNo < 0)slotNo = m_StartSlot;
			ZDx.GetDevContext()->VSSetConstantBuffers(slotNo, 1, &m_pBuffer);
		}
		
		//--------------------------------------------------------
		//   ピクセルシェーダに設定
		//   slotNo … 定数バッファをセットする場所(HLSLのregister(b0)とかの、b0のところの番号) -1でCreate()したときに指定した番号
		//--------------------------------------------------------
		void SetPS(int slotNo = -1)
		{
			if (m_pBuffer == nullptr)return;

			if (slotNo < 0)slotNo = m_StartSlot;
			ZDx.GetDevContext()->PSSetConstantBuffers(slotNo, 1, &m_pBuffer);
		}

		//--------------------------------------------------------
		//   ジオメトリシェーダに設定
		//   slotNo … 定数バッファをセットする場所(HLSLのregister(b0)とかの、b0のところの番号) -1でCreate()したときに指定した番号
		//--------------------------------------------------------
		void SetGS(int slotNo = -1)
		{
			if (m_pBuffer == nullptr)return;

			if (slotNo < 0)slotNo = m_StartSlot;
			ZDx.GetDevContext()->GSSetConstantBuffers(slotNo, 1, &m_pBuffer);
		}

		//--------------------------------------------------------
		//   コンピュートシェーダに設定
		//   slotNo … 定数バッファをセットする場所(HLSLのregister(b0)とかの、b0のところの番号) -1でCreate()したときに指定した番号
		//--------------------------------------------------------
		void SetCS(int slotNo = -1)
		{
			if (m_pBuffer == nullptr)return;

			if (slotNo < 0)slotNo = m_StartSlot;
			ZDx.GetDevContext()->CSSetConstantBuffers(slotNo, 1, &m_pBuffer);
		}

		//
		ZConstantBuffer() : m_pBuffer(0), m_StartSlot(-1), m_TypeFlag(BufType::None)
		{
		}
		//
		~ZConstantBuffer()
		{
			Release();
		}

	protected:
		ID3D11Buffer *		m_pBuffer;			// Direct3D11定数バッファ
		int					m_StartSlot;		// セットする定数バッファ番号
		BufType				m_TypeFlag;			// BufType
		int					m_BufferSize = 0;	// 作成されているバッファのサイズ
		
	private:
		// コピー禁止用
		ZConstantBuffer(const ZConstantBuffer& src) {}
		void operator=(const ZConstantBuffer& src) {}
	};

	#include "ZConstantBuffer.inl"

}
#endif
