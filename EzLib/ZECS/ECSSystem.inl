inline const ZSVector<uint32>& ECSSystemBase::GetComponentTypes()const
{
	return m_ComponentTypes;
}

inline const ComponentBitSet& ECSSystemBase::GetBitSet() const
{
	return m_CompBitSet;
}

inline const ZSVector<uint32>& ECSSystemBase::GetComponentFlags()const
{
	return m_ComponentFlags;
}

inline bool ECSSystemBase::IsActive()const
{
	if (m_IsEnable == false)
		return false;

	// コンポーネントを使用しないシステムなら無条件でtrue
	if (m_ComponentTypes.size() == 0)
		return true;

	for (uint32 i = 0; i < m_ComponentFlags.size(); i++)
	{
		if ((m_ComponentFlags[i] & ECSSystemBase::FLAG_OPTIONAL) == 0)
			return true;
	}

	return false;
}

inline bool ECSSystemBase::UseMultiThread()const
{
	return m_UseMultiThread;
}

inline ZString ECSSystemBase::GetSystemName()const
{
	return m_DebugSystemName;
}

template<typename T>
inline void ECSSystemBase::AddComponentType(uint32 compFlag)
{
	static_assert(is_base_of<__ECSComponentBase, T>(), "this class is not component");
	m_ComponentTypes.push_back(T::ID);
	m_CompBitSet.set(T::ID);
	m_ComponentFlags.push_back(compFlag);
}

template<typename ...Args>
inline void ECSSystemBase::AddMultiComponentType()
{
	constexpr size_t numParam = sizeof...(Args);
	if (numParam == 0) return;

	size_t IDs[] = { Args::ID ... };
	_AddMultiComponentType(IDs,numParam);
}

inline void ECSSystemBase::_AddMultiComponentType(size_t* IDs, size_t numIDs)
{
	if (IDs == nullptr || numIDs == 0)
		return;

	for (size_t i = 0; i < numIDs; i++)
	{
		m_ComponentTypes.push_back(IDs[i]);
		m_CompBitSet.set(IDs[i]);
		m_ComponentFlags.push_back(0);
	}
}

//template<class Component>
//inline Component* ECSSystemBase::GetCompFromUpdateParam(UpdateCompParams comp)const
//{
//	if (comp == nullptr)
//		return nullptr;
//
//	__ECSComponentBase* component = nullptr;
//
//	try
//	{
//		component = comp[m_UpdateCompIndexMap.at(Component::ID)];
//	}
//	catch (std::out_of_range&) // 例外(コンポーネントが登録されていなければ)
//	{
//		return nullptr;
//	}
//
//	return static_cast<Component*>(component);
//}

inline bool ECSSystemList::AddSystem(ZSP<ECSSystemBase> system)
{
	if (system->IsActive() == false)
		return false;

	m_Systems.push_back(system);
	return true;
}

template<typename T>
inline ZSP<T> ECSSystemList::AddSystem()
{
	static_assert(std::is_base_of<ECSSystemBase, T>(), "this class is not system");
	ZSP<T> system = Make_Shared(T,sysnew);
	m_Systems.push_back(system);
	return system;
}
