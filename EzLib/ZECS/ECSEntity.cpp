#include "ECSEntity.h"

namespace EzLib
{
namespace ZECS
{
	ECSEntity::~ECSEntity()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	void EzLib::ZECS::ECSEntity::RemoveAllEntity(ZAVector<ZSP<ECSEntity>>& entityList)
	{
		ZSVector<ZSP<ECSEntity>> removeTargetEntities;
		removeTargetEntities.reserve(entityList.size() / 2);
	
		// Handleがnullptrの要素を削除
		for (size_t i = 0; i < entityList.size(); i++)
		{
			if (entityList[i]->m_EntityHandle != NULL_ENTITY_HANDLE)
				removeTargetEntities.push_back(entityList[i]);
		}
	
		entityList.clear();

		// 昇順ソート
		std::sort(removeTargetEntities.begin(), removeTargetEntities.end(),
			[](ZSP<ECSEntity>& l, ZSP<ECSEntity>& r)
		{
			return l->m_EntityHandle < r->m_EntityHandle;
		});
	
		for (uint32 i = 0; i < removeTargetEntities.size(); i++)
			ECS.RemoveEntity(*removeTargetEntities[i].GetPtr());
	
		removeTargetEntities.clear();
		removeTargetEntities.shrink_to_fit();
	
	}

	void ECSEntity::Remove()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	ZSP<ECSEntity> ECSEntity::CreateEntityFromJsonFile(const ZString & fileName)
	{
		// json ファイル読み込み
		std::string error;
		json11::Json json = LoadJsonFromFile(fileName.c_str(), error);
		if (error.size() != 0)
		{
			DW_SCROLL(2, "json 読み込みエラー(%s)", fileName.c_str());
			return nullptr;
		}

		return std::move(CreateEntityFromJson(json));
	}

	ZSP<ECSEntity> ECSEntity::CreateEntityFromJson(const json11::Json & jsonObj)
	{
		// エンティティ作成
		auto entity = ECS.MakeEntity();

		for (auto& jsonComp : jsonObj["Components"].array_items())
		{
			ZString compName = jsonComp["ComponentType"].string_value().c_str();

			// 文字列からクラスインスタンスを生成する（ClassReflectionクラス使用）
			auto comp = ECS.InstantiateComponent(compName);

			// jsonで初期化
			comp.Instance->InitFromJson(jsonComp);

			// コンポーネント追加
			ECS.AddComponent(entity, comp);
		}

		return std::move(entity);
	}

}
}
