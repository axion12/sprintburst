#ifndef __ZMEMORY_ALLOCATOR_H__
#define __ZMEMORY_ALLOCATOR_H__

/// 参考 : https://github.com/hogehogei/TLSF_Allocator

// KByte -> Byte
constexpr size_t operator"" _KB(unsigned long long kbyte)
{
	return static_cast<size_t>(kbyte << 10);
}

// MByte -> Byte
constexpr size_t operator"" _MB(unsigned long long mbyte)
{
	return static_cast<size_t>(mbyte << 20);
}

// GByte -> Byte
constexpr size_t operator"" _GB(unsigned long long gbyte)
{
	return static_cast<size_t>(gbyte << 30);
}

size_t KB2Byte(size_t kb);

size_t MB2Byte(size_t mb);

size_t GB2Byte(size_t gb);

size_t Byte2KB(size_t byte);

size_t Byte2MB(size_t byte);

size_t Byte2GB(size_t byte);


namespace EzLib
{
namespace Memory
{
namespace BoundaryTag
{
	class BoundaryEndTag
	{
	public:
		BoundaryEndTag(size_t size);
		~BoundaryEndTag();

		// メモリブロックサイズ設定
		void SetSize(size_t size);

		// メモリブロックサイズ取得
		size_t GetSize()const;

	private:
		size_t m_MemorySize;

	};

	class BoundaryBeginTag
	{
	public:
		struct Header
		{
		public:
			Header(): Size(0), GeneralData(-1)
			{
			}
		
		public:
			// メンバ変数は必ずしも隙間なくメモリに配置されるわけではないっぽい(4バイト単位に区切られる?)
			// -> そのためsizeofで構造体のサイズを求めると実際のメンバ変数の合計サイズよりも多くなる
			size_t Size;
			short GeneralData;
			short IsUsed;
			size_t NumInstance;
		};
	
	public:
		BoundaryBeginTag();
		BoundaryBeginTag(size_t size);
		~BoundaryBeginTag();

		void SetFlag(bool isUsed);
		void SetSize(size_t size);
		void SetGeneralData(size_t data);
		void SetNumInstance(size_t num);

		bool Merge(BoundaryBeginTag* next);
		BoundaryBeginTag* Split(size_t size);

		BoundaryBeginTag* PrevTag();
		BoundaryBeginTag* NextTag();

		BoundaryEndTag* GetEndTag();

		void Detach();
		void SetNext(BoundaryBeginTag* p);
		void SetPrev(BoundaryBeginTag* p);
		BoundaryBeginTag* GetNext();
		BoundaryBeginTag* GetPrev();

		bool IsUsed()const;
		size_t GetSize()const;
		int GetGeneralData()const;
		size_t GetNumInstance()const;
		static constexpr size_t HeaderOverhead = sizeof(BoundaryBeginTag::Header);
		static constexpr size_t BlockOverhead = HeaderOverhead + sizeof(BoundaryEndTag);

	private:
		BoundaryBeginTag::Header m_Header;
		BoundaryBeginTag* m_Prev;
		BoundaryBeginTag* m_Next;
	};

	static constexpr size_t BBTAG_SIZE = sizeof(BoundaryBeginTag);
	static constexpr size_t BETAG_SIZE = sizeof(BoundaryEndTag);

	BoundaryBeginTag* NewTag(size_t size, uint8* p);
	void DeleteTag(BoundaryBeginTag* tag);
}

	class ZMemoryAllocator
	{
	public:
		struct AllocationInfo
		{
			size_t FreeMemory;
			size_t ActiveMemory;
			size_t AllBoundaryTagSize;
			size_t AllAllocSize;
			size_t AllFreeSize;
		};

	private:
		using BBeginTag = BoundaryTag::BoundaryBeginTag;
		using BEndTag = BoundaryTag::BoundaryEndTag;

		static const size_t Aligment;			// memory alignment
	public:
		ZMemoryAllocator();
		ZMemoryAllocator(size_t memorySize,size_t partitionBits = 4);
		~ZMemoryAllocator();
		
		// 初期化
		bool Init(size_t memorySize,size_t partitionBits = 4);
		
		// 解放
		void Release();

		// メモリ再確保
		bool ReAlloc();

		// メモリ確保
		template<typename T>
		void* Malloc(size_t size);
		template<typename T>
		void* Malloc(size_t size, int generalData);

		// メモリ解放
		template<class T>
		void Free(T* p);
		
		template<>
		void Free<void>(void* p);

		// メモリが使用されているか
		static bool IsActivePtr(void* p);

		// ポインタが参照しているメモリのサイズ取得
		static size_t GetPtrMemSize(void* p);

		// メモリブロックのヘッダにある汎用データ取得
		static int GetGeneralData(void* p);

		// メモリ情報取得
		const AllocationInfo& GetAllocationInfo()const;

		// 確保したメモリサイズ取得
		const size_t GetMemorySize()const;
	private:
		// メモリ切り出し
		BBeginTag* GetFreeBlock(size_t size);
		uint8* _Malloc(size_t size,size_t numInstance);
		uint8* _Malloc(size_t size, size_t numInstance, int generalData);

		// 2^x <= sizeとなるxを返す
		size_t GetMSB(size_t size)const;
		// 2^x >= sizeとなるxを返す
		size_t GetLSB(size_t size)const;
		
		// fli,sliを元にフリーブロックを検索
		size_t CalcFreeBlockIndex(size_t fli, size_t sli)const;

		size_t GetSLI(size_t size, size_t msb)const;

		bool GetMinFreeListBit(size_t bit, size_t freeListBit, size_t* result)const;

		// 設定したアラインメントの倍数取得
		size_t GetAlignmentedSize(size_t size)const;

		void GetIndex(unsigned size, unsigned* fli, unsigned* sli)const;

		void ClearBit(unsigned index, unsigned fli, unsigned sli);

		// サイズをもとにフリーブロックを検索
		BBeginTag* SerchFreeBlock(size_t size);

		// フリーブロックリストから除外
		void UnRegisterFreeBlock(BBeginTag* beginTag);
		// フリーブロックリストに登録
		void RegisterFreeBlock(BBeginTag* beginTag);

		// 任意のブロックの前後のブロックと可能ならマージする
		BBeginTag* MergeFreeBlock(BBeginTag* block);
		
		// 確保したブロックを解放
		void DeAllocateBlock(BBeginTag* block);

	private:
		AllocationInfo m_AllocationInfo;		// メモリ情報
		std::mutex m_Mtx;						// アクセスを単一スレッドからのみに制限

		size_t m_SLIPartitionBits;				// SLIのビット数
		size_t m_SLIPartition;					// SLI分割数
		size_t m_MinAllocSize;					// 最小メモリ割り当てサイズ

		uint8* m_Base;							// 確保したメモリの先頭アドレス
		size_t m_MemorySize;					// 確保したメモリのサイズ
//		std::vector<BBeginTag*> m_FreeBlocks;
		BBeginTag** m_pFreeBlocks;				// 使用可能なブロック
		size_t m_FreeListBitFLI;				// FLIのFreeListフラグ
//		std::vector<uint32> m_FreeListBitSLI;
		uint32* m_pFreeListBitSLI;				// SLIのFreeListフラグ

		bool m_IsInitialized;

	};

}
}

namespace EzLib
{
namespace Memory
{
#include "ZMemoryAllocator.inl"
}
}


#endif