//===============================================================
//  @file ZBullet_RigidBody.h
//   「Bullet Physics Engine」の「剛体」関係のクラス
// 
//  適当にまとめて書いてます。
// 
//  @author 鎌田
//===============================================================
#ifndef ZBullet_RigidBody_h
#define ZBullet_RigidBody_h

#pragma warning(disable:4316)

namespace EzLib
{
	class ZMotionState;
	
	//==========================================================================
	//   Bullet剛体基本クラス
	//  実際に物理演算で動く物体。Shapeクラスを引っ付けると、その形状で動作する。
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsRigidBody : public ZPhysicsObj_Base
	{
	public:


		//========================================================================
		// 作成・解放
		//========================================================================

		//   形状と剛体を作成
		// 
		//  	shape		… 登録する形状
		//  	startMat	… 初期行列(拡大は含めれない)
		//  	mass		… 質量
		void Create(ZSP<ZPhysicsShape_Base> shape, const ZMatrix& startMat, float mass);

		//   解放
		virtual void Release() override;

		//   ワールドへこの剛体を追加する ZPhysicsWorld::AddRigidBodyでも可能
		//  	group		… 判定グループフィルタ(所属しているグループにのみ衝突を行う)
		//  	mask		… 判定マスクフィルタ(指定groupの剛体と衝突するか？のフィルタ 0xFFFFで全ての剛体と衝突する)
		bool AddToWorld(ZPhysicsWorld& world, short group = 1, short mask = -1);

		//   ワールドから剛体を解除する
		bool RemoveFromWorld();


		//========================================================================
		// 取得・設定
		// ※一部だけです
		//========================================================================

		//   剛体取得
		sptr<btRigidBody>		GetBody() { return m_Body; }

		//   キネマティック設定
		//  	kinematic	… キネマティックにするか？
		void SetKinematic(bool kinematic)
		{
			if (kinematic)
			{
				m_Body->setCollisionFlags(m_Body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
				m_Body->setActivationState(DISABLE_DEACTIVATION);
				m_Body->setMassProps(0, btVector3(0, 0, 0));
				m_Mass = 0;
				m_Body->updateInertiaTensor();
			}
			else
			{
				m_Body->setCollisionFlags(m_Body->getCollisionFlags()& ~btCollisionObject::CF_KINEMATIC_OBJECT);
				m_Body->setActivationState(ACTIVE_TAG);
				m_Body->setMassProps(m_Mass, m_Inertia);
				m_Body->updateInertiaTensor();
			}
		}

		//   静的剛体か？(動かないもの)
		bool IsStatic()
		{
			if (m_Body->getCollisionFlags()& btCollisionObject::CF_KINEMATIC_OBJECT)
			{
				return true;
			}
			return false;
		}

		//   質量
		void SetMass(float mass)
		{
			m_Mass = mass;
			if (m_Body->getCollisionFlags()& btCollisionObject::CF_KINEMATIC_OBJECT)
			{
				m_Body->setMassProps(0, btVector3(0, 0, 0));
			}
			else
			{
				m_Body->setMassProps(m_Mass, m_Inertia);
			}
			m_Body->updateInertiaTensor();
		}

		//   質量取得
		float GetMass() { return m_Mass; }

		//   慣性テンソル更新
		void UpdateInertia()
		{
			if (m_Shape == nullptr)return;

			if (m_Body->getCollisionFlags()& btCollisionObject::CF_KINEMATIC_OBJECT)
			{
				m_Body->setMassProps(0, btVector3(0, 0, 0));
			}
			else
			{
				// 慣性テンソル更新
				if (m_Mass != 0.0f)
				{
					m_Shape->GetShape()->calculateLocalInertia(m_Mass, m_Inertia);
				}
				m_Body->setMassProps(m_Mass, m_Inertia);
			}
			m_Body->updateInertiaTensor();
		}
		//   反発力設定
		void SetRestitution(float r)
		{
			m_Body->setRestitution(r);
		}
		//   摩擦力設定
		void SetFriction(float f)
		{
			m_Body->setFriction(f);
		}
		//   移動減衰設定
		void SetLinearDamping(float d)
		{
			m_Body->setDamping(d, m_Body->getAngularDamping());
		}
		//   回転減衰設定
		void SetAngularDamping(float d)
		{
			m_Body->setDamping(m_Body->getLinearDamping(), d);
		}
		//   移動力設定
		void SetLinearVelocity(const ZVec3& v)
		{
			m_Body->setLinearVelocity(btVector3(v.x, v.y, v.z));
		}
		//   回転力設定
		void SetAngularVelocity(const ZVec3& v)
		{
			m_Body->setAngularVelocity(btVector3(v.x, v.y, v.z));
		}

		//   かかる力をクリアする
		void ClearForces()
		{
			m_Body->clearForces();
		}
		//   回転力を与える
		void ApplyTorque(const ZVec3& t)
		{
			m_Body->applyTorque(btVector3(t.x, t.y, t.z));
		}
		//   力を与える(継続的な力。シェット噴射のような挙動)
		void ApplyForce(const ZVec3& force)
		{
			m_Body->applyCentralForce(btVector3(force.x, force.y, force.z));
		}
		//   指定位置に力を与える(継続的な力。シェット噴射のような挙動)
		void ApplyForce(const ZVec3& force, const ZVec3& rel_pos)
		{
			m_Body->applyForce(btVector3(force.x, force.y, force.z), btVector3(rel_pos.x, rel_pos.y, rel_pos.z));
		}
		//   力を与える(瞬間的な力。バットで打った時のような挙動)
		void ApplyImpulse(const ZVec3& impulse)
		{
			m_Body->applyCentralImpulse(btVector3(impulse.x, impulse.y, impulse.z));
		}
		//   指定座標に力を与える(瞬間的な力。バットで打った時のような挙動)
		void ApplyImpulse(const ZVec3& impulse, const ZVec3& rel_pos)
		{
			m_Body->applyImpulse(btVector3(impulse.x, impulse.y, impulse.z), btVector3(rel_pos.x, rel_pos.y, rel_pos.z));
		}
		//   スリープしなくする
		void SetNonSleep()
		{
			m_Body->setSleepingThresholds(0, 0);
			//		m_Body->setActivationState(DISABLE_DEACTIVATION);
		}

		//   剛体へ直接行列をセットする
		//  	mat		… 設定したい行列
		void SetMatrix(const ZMatrix& mat)
		{

			// スケールは形状の方へセット
			if (m_Shape)
			{
				m_Shape->SetLocalScaling(mat.GetScale());
			}

			// 剛体の行列はスケールを含めない
			ZMatrix m(mat);
			m.NormalizeScale();

			// Bullet用の行列へ変換
			btTransform t;
			t.setFromOpenGLMatrix(&m._11);
			// セット
			m_Body->getMotionState()->setWorldTransform(t);
			m_Body->setCenterOfMassTransform(t);
		}
		//   剛体から行列を取得する
		//  形状を持ってる場合は、その大きさも行列に含む
		//  @param[out]	mOut	… 取得した行列を入れるための変数
		void GetMatrix(ZMatrix& mOut)
		{
			// 行列取得
			btTransform t = m_Body->getWorldTransform();
			t.getOpenGLMatrix(&mOut._11);
			if (m_Shape)
			{
				ZVec3 scale;
				m_Shape->GetLocalScaling(scale);
				mOut.Scale_Local(scale);
			}
		}

		//========================================================================
		// モーションステート
		//========================================================================

		//   モーションステート作成
		template<class T>
		T* CreateMotionState();

		//   モーションステートをセット
		void SetMotionState(btMotionState* ms);

		// アクティブ時用のモーションステートを任意のものに変更
		void SetActiveMotionState(ZSP<ZMotionState>& motionState)
		{
			m_ActiveMotionState = motionState;
		}

		// アクティブ時用のモーションステートを取得
		auto GetActiveMotionState()
		{
			return m_ActiveMotionState;
		}

		// Kinematic時用のモーションステートを任意のものに変更
		void SetKinematicMotionState(ZSP<ZMotionState>& motionState)
		{
			m_KinematicMotionState = motionState;
		}

		// Kinematic時用のモーションステートを取得
		auto GetKinematicMotionState()
		{
			return m_KinematicMotionState;
		}

		void SetActivationMotionState(bool activation);
		
		//========================================================================
		// 衝突判定用グループ関係
		//========================================================================

		// グループ設定
		void SetGroup(uint16 group)
		{
			m_Group = group;
			
			if (m_World == nullptr)
				return;
			
			// 衝突グループの変更法がよくわからないので一度物理ワールドから排除し登録し直す
			m_World->GetWorld()->removeCollisionObject(m_Body.get());
			m_World->GetWorld()->addRigidBody(m_Body.get(), m_Group, m_UnCollisionGroup);
		}

		// グループ取得
		uint16 GetGroup()
		{
			return m_Group;
		}

		void SetUnCollisionGroup(uint16 unCollisionGroup)
		{
			m_UnCollisionGroup = unCollisionGroup;
			
			if (m_World == nullptr)
				return;
			
			// 衝突グループの変更法がよくわからないので一度物理ワールドから排除し登録し直す
			m_World->GetWorld()->removeCollisionObject(m_Body.get());
			m_World->GetWorld()->addRigidBody(m_Body.get(), m_Group, m_UnCollisionGroup);
		}

		// 非衝突グループ取得
		uint16 GetUnCollisionGroup()
		{
			return m_UnCollisionGroup;
		}


		// 
		ZPhysicsRigidBody() : m_Body(nullptr), m_Inertia(0, 0, 0), m_Mass(0)
		{
		}
		// 
		virtual ~ZPhysicsRigidBody()
		{
			Release();
		}

	protected:
		btVector3					m_Inertia;	// 慣性テンソル
		sptr<btRigidBody>			m_Body;		// 剛体
		float						m_Mass;		// 質量

		ZSP<ZPhysicsShape_Base>		m_Shape;				// 設定されてる形状
		ZSP<ZMotionState>			m_ActiveMotionState;	// アクティブ時に使用するMotionState
		ZSP<ZMotionState>			m_KinematicMotionState;	// Kinematic時に使用するMotionState
		
		uint16						m_Group;
		uint16						m_UnCollisionGroup;

	};

	bool CreateRigidBody(ZPhysicsRigidBody** out, ZBP_RigidBody& rbBp);

	class ZMotionState : public btMotionState
	{
	public:
		virtual void Reset() = 0;
		virtual void ReflectGlobalTransform() = 0;
		virtual btTransform GetInitTrans() = 0;
	};

	class ZDefaultMotionState : public ZMotionState
	{
	public:
		ZDefaultMotionState(const ZMatrix& transform)
		{
			m_Trans.setFromOpenGLMatrix(&transform._11);
			m_InitTrans = m_Trans;
		}

		void getWorldTransform(btTransform& worldTransform)const override
		{
			worldTransform = m_Trans;
		}

		void setWorldTransform(const btTransform& worldTransform)override
		{
			m_Trans = worldTransform;
		}

		virtual void Reset()override
		{
			m_Trans = m_InitTrans;
		}

		virtual void ReflectGlobalTransform()override
		{
		}

		virtual btTransform GetInitTrans()override
		{
			return m_InitTrans;
		}

	private:
		btTransform m_InitTrans;
		btTransform m_Trans;
	
	};
	


}


#endif
